import LabelData
import TweetTokenizer
import DocumentVector

from gensim.models import Doc2Vec
# from gensim.models.doc2vec import LabeledSentence
import json

import pandas as pd

import redis
import TesterModel
import math

from textblob import TextBlob


def getLabeledData(symbol):
    labeledData = LabelData.label(symbol)
    return labeledData

def transformLabeledData(symbol = None, dataset=None):

    if dataset==None:
        labeledData = getLabeledData(symbol)
    else:
        labeledData = dataset

    documents = []
    labels = []
    # labeleds = []
    timestamps = []
   
    ids = []
    for i in labeledData:
        
        if "text" in i:
            
            tokens = TweetTokenizer.tokenize(i["text"])
           
            if len(tokens)>0 and tokens != "NC":
                ids.append(i["id"])
                # documents.append(tokens)
                # somehow keras has their own approach to read the text
                # documents.append(" ".join(tokens))
                documents.append(tokens)
                # labels.append({i["id"]:i["label"]}) #uncomment for doc2vec
                labels.append(i["label"])
                """
                    StockTwits doesn't have timestamp_ms
                    use : timestamps.append(i["created_at_date"])
                """
                # Twitter
                timestamps.append(i["created_at"])

            # labeleds.append(LabeledSentence(words=tokens, tags=[i["id"]]))

    # model = DocumentVector.documentVectorizer(labeleds) #doc2vec creator (uncomment this!!!)
    return {"labels":labels,"documents": documents, "id":ids, "timestamps": timestamps}

def generateDataset():

    id_vector_label = []
    postive_labels = []
    negative_labels = []

    model = TesterModel.getModel()

    labels = transformLabeledData()
    for i in labels["labels"]:
        key = list(i.keys())
        value = i[key[0]]
        if key[0] in model.docvecs:

            id_vector_label.append([key[0],model.docvecs[key[0]],value]) #id,vector,label
            # if value == "negative":
            if value < 0:
                negative_labels.append({key[0]: [model.docvecs[key[0]],'negative']})
            # elif value =="positive":
            elif value > 0:
                postive_labels.append({key[0]: [model.docvecs[key[0]],'positive']})
    
    storeFullDataset(id_vector_label)

def storeDataset(trainX, trainY, testX, testY):

    r = redis.StrictRedis(host='localhost', port=6379)

    # r.set('trainX1', pd.Series(trainX).to_json(orient='values'))
    # r.set('trainY1', pd.Series(trainY).to_json(orient='values'))

    # r.set('testX1', pd.Series(testX).to_json(orient='values'))
    # r.set('testY1', pd.Series(testY).to_json(orient='values'))

def storeFullDataset(X):

    r = redis.StrictRedis(host='localhost', port=6379)

    # r.set('X', pd.Series(X).to_json(orient='values'))

    r.set('X_DJIA', pd.Series(X).to_json(orient='values'))

