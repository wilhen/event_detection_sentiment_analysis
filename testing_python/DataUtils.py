import redis
import json
import pandas as pd 

def setTempDateEvent(X, key):

    r = redis.StrictRedis(host='localhost', port=6379)

    r.set(key, pd.Series(X).to_json(orient='values'))

def getTempDateEvent(key):

    r = redis.StrictRedis(host='localhost', port=6379)

    return json.loads(r.get(key))