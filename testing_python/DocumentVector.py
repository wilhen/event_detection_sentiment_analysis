import gensim
import numpy
import logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

def documentVectorizer(documents):
    print(len(documents))
    model = gensim.models.Doc2Vec(size=100, seed=1337, dbow_words= 1, dm=0, iter=1, window=8, min_count=50, 
        workers=11, alpha=0.025, min_alpha=0.025) # use fixed learning rate

    model.build_vocab(documents)
    # total_words = sum([len(i) for i in documents])
    total_examples = len(documents) 
    for epoch in range(10):
        print ("epoch: "+ str(epoch))
        model.train(documents, total_examples=total_examples, epochs= 1)
        model.alpha -= 0.002  # decrease the learning rate
        # model.min_alpha = model.alpha  # fix the learning rate, no decay
    
    print ("Model has been successfully made")
    # model.save('~/Documents/testing_python/tweets_model_dumped2b.doc2vec')
    model.save('~/Documents/testing_python/tweets_djia_model_dumped.doc2vec')

    return model