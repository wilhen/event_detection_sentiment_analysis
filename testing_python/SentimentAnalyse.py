from DatasetUtils import transformLabeledData

import numpy as np
import dill
from sklearn.externals import joblib
import redis
import json
import sys, getopt
from TwitterData import label
from nltk.util import ngrams
from TweetFiltering import n_grams_analyser
from OutlierDataset import vector_analyser
import pandas as pd
from TfIdfVectorizer import analyser
from sklearn.neighbors import LocalOutlierFactor
from OutlierDataset import getTwitterAttibutes
from LSH import lsh_function
from datetime import datetime

from pymongo import MongoClient
import urllib3 
import pickle


red = redis.StrictRedis(host='localhost', port=6379)

# date= "2018-01-26"
def extractLSHInfo(lsh_info):
    # sampling from LSH bucket
    temp = []
    for i in lsh_info.keys():
        temp.append(lsh_info[i][0])

    return temp

def active_learning(clf, X, t = 1.):
    ### active learning
    d_func = np.abs(clf.decision_function(X))
    al_idx = np.argsort(d_func)
    _index = []
    _sorted_d_func = d_func[al_idx]
    print (al_idx)
    print (_sorted_d_func)
    _filtered_d_func = []
    for i in range(len(_sorted_d_func)):
        if _sorted_d_func[i] < t:
            _filtered_d_func.append((al_idx[i], _sorted_d_func[i]))
            _index.append(al_idx[i])
    print(_filtered_d_func)

    return _filtered_d_func, _index

    


def getLSHInfo(documents, labels, ids, date):

    lsh_info = lsh_function(documents, labels, ids, date)
    
    lsh_info = filter_group_outlier(lsh_info)
   
    return extractLSHInfo(lsh_info)


def filter_group_outlier(lsh_info):
    df = pd.read_csv("/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/data/group_outlier.csv", delim_whitespace=True, header=None)
    dataset = df.values
   

    for i in dataset:
        # keys.append(i[0])
        if i[0] in lsh_info:
            lsh_info.pop(i[0])

    return lsh_info


def filter_outlier(documents, labels, ids, date):
    df = pd.read_csv("/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/data/outliers.csv", delim_whitespace=True, header=None)
    dataset = df.values
    # print(documents)
    lsh_info = getLSHInfo(documents, labels, ids, date)
   
    vect = joblib.load("/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/OutlierDataset/tfidf_4_5_2018.pkl")
    mi = joblib.load("/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/OutlierDataset/selector_4_5_2018.pkl")
    rf = joblib.load("/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/OutlierDataset/rf_4_5_2018.pkl")
    
    docs=[]
    indexes = []
    new_docs = []

    """
        The supervised anomaly detection
    """

    # tfidf2 = vect.transform(documents)
    # X_new = mi.transform(tfidf2)
    # prds = rf.predict(X_new)
    # d2 = []
    # for i in range(len(prds)):
    #     if prds[i]==0:
    #         d2.append(documents[i])
    # print (d2)
    # documents = d2
    
    
    for i in range(len(documents)):
        if ids[i] not in lsh_info:
            continue    
        doc = documents[i]
        n_grams = ngrams(doc,2)
        ndoc = [ '_'.join(grams) for grams in n_grams]
        if len(ndoc)>0:
            docs.append(ndoc)
            # legitimate document
            new_docs.append(documents[i])
    
    # this one is not necessary since the documents are only one. To prevent any ambiguity, 
    # ids= lsh_info is removed
    # because if id is in lsh bucket, then that item is the sample of the buckets population
    # ids = lsh_info

    results = []
    if len(docs) ==0:
        return results, indexes
    
    """Uncomment belows: Isolation Forest"""
    tfidf = joblib.load("/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/tfidf_GE_v0_08_04_2018.pkl")
    vectors = tfidf.transform(docs)

    iso_forest = joblib.load("/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/iso_GE_v0_08_04_2018.pkl")
    pred = iso_forest.predict(vectors)
    """Stop here"""

    
    # lbl = []
    counter = 0
    documents = new_docs
    for i in range(len(pred)): # len(pred) == len(documents) == len(ids)
        if pred[i] == 1:
            illegal = ["crypocurrence", "crypto", "live", "jessica", '68,535',  "feedback",  "bout" , "account", "bitcoin","pulsestream",  "link","sharma" ,"lichtenfeld" ,"james", "jason", "harness", "revolution", "bftd", "candy" ,"video", "hangout", "prophecy", "altruism","_results_" ,"assisstance", "seema","'whistle-blower","sapra", "blockchain", "question"]
            passes = True

            if "rare" in documents[i] and "moment" in documents[i] and "life" in documents[i]:
                passes= False

            if "hey" in documents[i] and "everything" in documents[i] and "trade" in documents[i]:
                passes= False

            if "trade" in documents[i] and "idea" in documents[i] and "chart" in documents[i]:
                passes= False
            
            if "abandon" in documents[i] and "humility" in documents[i] and "misfortune" in documents[i]:
                passes= False

            if "gorgeous" in documents[i] and "chart" in documents[i]:
                passes= False
            
            if "potential" in documents[i] and "jewel" in documents[i]:
                passes= False

            for ii in illegal:
                if ii in documents[i]:
                    if ii =="link":
                        if "open" in documents[i] or "sign" in documents[i]:
                            passes = False
                        
                    passes = False

            if  [int(ids[i])] in dataset or  passes== False:
                continue
            else:
                # print (ids[i])
                # print (documents[i])
                results.append(documents[i])
                indexes.append(ids[i])
      
        else:
            
            counter+=1
    print ("outliers ", counter)
    return results, indexes

def serialise_date(timestamps):

    ts = []

    for t in timestamps:
        if type(t) != str:
            ts.append(t.strftime("%Y-%m-%d"))

    return ts

def errorNotification(e):

    webhook_url = "https://hooks.slack.com/services/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"
    http = urllib3.PoolManager()

    body = {"channel":"williamhenry", "username": "Stoxly", "text": "```Sentiment Analysis Error \n "+str(e)}
    json_body  = json.dumps(body)
    r = http.request("POST", webhook_url, headers={'Content-Type': 'application/json'}, body=json_body )


def analyse_sentiment(date, active=False, data=None, forced=False):

    quotedStatus_tickers = list()
    status_tickers = list()
    uuid = None

    if data == None:
        data = label("GE", date)
    else:
        d = data.decode("utf-8")
        d= json.loads(d)
       
        # strip off data
        uuid = d["uuid"]
        date = d["created_at"]
        time = datetime.strptime(date, "%Y-%m-%d %H:%M:%S")
        date_temp = time
        date = time.date().strftime("%Y-%m-%d")
        tickers = d["tickers"]
        quotedStatus_tickers = tickers["quoted_status"]
        status_tickers = tickers["status"]

        data = label(None, None, data=[d])
        print (data)


    dataset = transformLabeledData(dataset=data)
    
    documents = dataset["documents"]
    labels = dataset["labels"]
    ids = dataset["id"]
    print ("document is")
    print (documents)

    if len(documents) == 0:
        # don't do anything
        return

    # dataset_ = red.get("dataset-"+date)
    
    # if dataset_ != None:
    #     print ("key updating ..")
    #     dataset_ = json.loads(dataset_)
    #     documents_ = dataset_["documents"]
        
    #     ids_ = dataset_["id"]
    #     labels_ = dataset_["labels"]
    #     if len(documents)>0 and len(ids)>0:
    #         # more protection
    #         if ids[0] not in ids_:
    #             documents_.append(documents[0])
    #             ids_.append(ids[0])
                
    #             labels_.append(labels[0])
    #             timestamps_ = dataset_["timestamps"]
    #             timestamps_.append(dataset["timestamps"][0])
    #             timestamps_ = serialise_date(timestamps_)
    #             n_data = {"labels": labels_, "documents":documents_, "id":ids_,"timestamps":timestamps_}
    #             red.set("dataset-"+date, json.dumps(n_data))

    #     documents = documents_
    #     ids = ids_
    #     labels = labels_

    # else:
    #     print ("key init")
    #     timestamps_ = dataset["timestamps"]
    #     timestamps_ = serialise_date(timestamps_)
    #     n_data = {"labels": labels, "documents":documents, "id":ids,"timestamps":timestamps_}
    #     red.set("dataset-"+date, json.dumps(n_data))


    vectorizer = joblib.load("/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/tfidf_vectorizer_x0_14_03_2018.pkl")

    ch2 = joblib.load("/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/chi2Best_x0_14_03_2018.pkl")
    # clf = joblib.load("./pickles/svm_x0_25_03_2018.pkl")
    ### Try this one
    clf = joblib.load("/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/ROC_based/roc_svm_beta3.pkl")


    # iso_vect = joblib.load("./pickles/OutlierDataset/tfidf_4_5_2018.pkl")

    # iso_mi = joblib.load("./pickles/OutlierDataset/selector600_5_5_2018.pkl")
    # lsa = joblib.load("./pickles/OutlierDataset/lsa_6_5_2018.pkl")
    # iso = joblib.load("./pickles/OutlierDataset/iso_5_5_2018.pkl")
    iso = joblib.load("/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/OutlierDataset/iso_text_feature_7_5_2018.pkl")

    # filtered
    docs, indexes = filter_outlier(documents, labels, ids, date)
    # docs = documents
    # indexes = ids
    if len (docs) ==0 or  len(indexes)==0:
        return None

    vectors = vectorizer.transform(docs)

    tfidf_feature_names = vectorizer.get_feature_names()
   

    tfidf = dict(zip(tfidf_feature_names, vectorizer.idf_))
    X = ch2.transform(vectors)

    # iso_vectors = iso_vect.transform(docs)
    # X_v = lsa.transform(iso_vectors)
    col2 = []

    for i in indexes:
        attrs = getTwitterAttibutes(i)
        col2.append(attrs)

    col2 = np.array(col2)
    iso_pred = iso.predict(col2)

    # lof = LocalOutlierFactor(n_neighbors=10, algorithm="auto",  metric="cosine", contamination=0.2, n_jobs=2)
    # lof_pred = lof.fit_predict(vectors)

    if tfidf_feature_names:
        # keep selected feature names
        tfidf_feature_names = [tfidf_feature_names[i] for i
                            in ch2.get_support(indices=True)]
        # print(tfidf_feature_names)

    if active:
        doc2 = []
        id2 = []
    
        al, _index=  active_learning(clf, X)
        for i in al:
            doc2.append(docs[i[0]])
            id2.append(indexes[i[0]])
      
        docs = doc2
        indexes = id2
        X = X[_index]

    ### prediction
    pred = clf.predict(X)

    res = []

    # analyse result
    one = []
    zero = []
    for i in range(len(pred)):
        if iso_pred[i] >0:
            if pred[i] == 1:
                # print("Bullish")
                # print (docs[i])
                one.append(i)
                res.append({"sentiment": "bullish", "tokens":docs[i], "id": indexes[i]})
            else:
                # print("Bearish")
                # print (docs[i])
                zero.append(i)
                res.append({"sentiment": "bearish", "tokens":docs[i], "id": indexes[i]})
    # print (one)
    # print(zero)
    # print (len(one))
    # print (len(zero))

    client = MongoClient('10.152.0.3',27017)
    db = client.SentimentAnalysis
    sa = db.daily_sentiment
    
    ticker_index = [] # if a symbol in each list, update only one per tweet
    if not forced:
        create_dt = datetime.now()
    else:
        create_dt = date_temp

    if len(one) ==0 and len(zero)==0:
        return None
    for st in status_tickers:
       
        col_set = dict()
        col_set["positives"] = len(one)
        col_set["negatives"] = len(zero)
        col_set["id"] = st + uuid
        col_set["symbol"] = st
        col_set["created_at"] = create_dt
        col_set["updated_at"] = create_dt
        if st in ticker_index:
            continue
        else:
            ticker_index.append(st)

        if sa.find({"id":col_set["id"]}).count() ==0:
            sa.insert(col_set)
        else:

            tmp = sa.find_one({"id":col_set["id"]})
            positives = tmp["positives"] + col_set["positives"]
            negatives = tmp["negatives"] + col_set["negatives"]
            
            sa.find_one_and_update({"id":col_set["id"]},{"$set": {"updated_at":datetime.now(), "negatives": negatives, "positives":positives}})
    
    for st in quotedStatus_tickers:
       
        col_set = dict()
        col_set["positives"] = len(one)
        col_set["negatives"] = len(zero)
        col_set["symbol"] = st
        col_set["id"] = st + uuid
        col_set["created_at"] = create_dt
        col_set["updated_at"] = create_dt
        if st in ticker_index:
            continue
        else:
            ticker_index.append(st)

        if sa.find({"id":col_set["id"]}).count() ==0:
            sa.insert(col_set)
        else:
            tmp = sa.find_one({"id":col_set["id"]})
            positives = tmp["positives"] + col_set["positives"]
            negatives = tmp["negatives"] + col_set["negatives"]

            sa.find_one_and_update({"id":col_set["id"]},{"$set": {"updated_at":datetime.now(), "negatives": negatives, "positives":positives}})
           
    # won't be used anylonger
    # with open('/Users/williamhenry/Desktop/data.json', 'w') as outfile:
    #     print("printing result")
    #     json.dump(res, outfile)

# main()


def run_background(data):
    dt = datetime.now()
    client = MongoClient('10.152.0.3',27017)
    db = client.BatchLog
    logs = db.sentiment_log
    try:
        # errorNotification("Process Run "+ data)
        analyse_sentiment(None, active=False, data=data)
        log = {"status":"Sentiment Analysis Run","message": "Success", "updated_at": dt}
        logs.insert(log)
    
    except Exception as e:
       errorNotification(e)
       log = {"status":"Sentiment Analysis error","message": str(e), "updated_at": dt}
       logs.insert(log)



def main(argv):
    date = ''
    active = False
    try:
        opts, args = getopt.getopt(argv,"hi:u",["date=", "active="])
    except getopt.GetoptError:
        print ('SentimentAnalyse.py --date=<date> --active=<active>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print ('SentimentAnalyse.py --date=<date> --active=<active>')
            sys.exit()
        
        elif opt in ("-d", "--date"):
            date = arg
        elif opt in ("-a", "--active"):
            active = arg
        
    print ('Date is', date)
    print ("is active", bool(int(active)))
    date = date.strip()
    active = bool(int(active))
    analyse_sentiment(date, active= active)


# if __name__ == "__main__":
#     main(sys.argv[1:])
