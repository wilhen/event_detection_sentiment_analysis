import redis
import json

import DatasetUtils  
import dill
from sklearn.externals import joblib
from TfIdfVectorizer import analyser
from sklearn.svm import SVC
from sklearn.metrics import precision_score, recall_score, confusion_matrix, f1_score

import pandas as pd

import numpy as np

from pymongo import MongoClient
from LabelData import processTweets

from pprint import pprint

red = redis.StrictRedis(host='localhost', port=6379)

no_features = 100

def storeLeftOvers(data):
    print ("storing left overs from idx: 2500")
    leftovers = []
    for i in data:
        newObj = dict()
        newObj["id"] = i["id"]
        newObj["text"] = i["message"]
        newObj["label"] = i["sentiment"]
        newObj["created_at"] = i["created_at"]
        newObj["is_reshare_message"] = i["is_reshare_message"]
        leftovers.append(newObj)

    red.set("leftovers", json.dumps(leftovers))

def getAmazon():
    client = MongoClient("localhost", 27017)
    db = client.tweets
    stream_gcp = db.stream_gcp_2
    datasets  = list(stream_gcp.find())
    datasets_size = stream_gcp.count()
    print(datasets_size)
    tweets = processTweets(datasets,datasets_size)
    return tweets


def getLeftOvers():

    leftovers = getAmazon()
    dataset = DatasetUtils.transformLabeledData(dataset=leftovers)

    documents = dataset["documents"]
    labels = dataset["labels"]
   
    vectorizer = joblib.load("./pickles/tfidf_vectorizer_x1_07_03_2018.pkl")

    ch2 = joblib.load("./pickles/chi2Best_x1_07_03_2018.pkl")
    clf = joblib.load("./pickles/svm_x1_07_03_2018.pkl")
    # clf = joblib.load("./pickles/rf_x0_12_03_2018.pkl")
    

    # yt = []
    # for i in labels:
    #     # xt.append(i[1])
    #     # twitter: positive
    #     if i == "Bullish" :
    #         yt.append(1)
    #     else:
    #         yt.append(0)

    # yt = np.array(yt)

    vectors = vectorizer.transform(documents)

    tfidf_feature_names = vectorizer.get_feature_names()
    # print (tfidf_feature_names)

    tfidf = dict(zip(tfidf_feature_names, vectorizer.idf_))
    # print (tfidf)
    # print(vectors.shape)
    """
        Recreate: xt, yt, feature_names = extractBestFeature(tfidf_feature_names, vectors, yt)
    """
    X = ch2.transform(vectors)

    if tfidf_feature_names:
        # keep selected feature names
        tfidf_feature_names = [tfidf_feature_names[i] for i
                            in ch2.get_support(indices=True)]
        print(tfidf_feature_names)
    

    
    pred = clf.predict(X)
    print (pred)
    one = []
    zero = []
    for i in range(len(pred)):
        if pred[i] == 1:
            print("Bullish")
            print (documents[i])
            one.append(i)
        else:
            print("Bearish")
            print (documents[i])
            zero.append(i)
    # print (one)
    # print(zero)
    print (len(one))
    print (len(zero))

    """
    ### Classsifier Stuffs
    print ("SVM classifier")
    print ("Accuracy Score:")
    # print (clf.score(X,yt))
    print ("Precision Score:")
    # print (precision_score(yt, pred))
    print ("Recall Score:")
    # print (recall_score(yt, pred))
    print ("F1 Score:")
    # print (f1_score(yt, pred))
    print ("Confusion Matrix:")
    # conf = confusion_matrix(yt,pred)
    # print (conf)
    # tn, fp, fn, tp = conf.ravel()
    # print ((tn, fp, fn, tp))
    """

 

def main():
    getLeftOvers()

if __name__ == "__main__":
    main()

