from DatasetUtils import transformLabeledData
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Embedding, LSTM
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from numpy import array
from numpy import asarray
from numpy import zeros
from sklearn.model_selection import train_test_split
from imblearn.over_sampling import SMOTE, RandomOverSampler, ADASYN
from sklearn.metrics import precision_score, recall_score, confusion_matrix, f1_score
from keras import regularizers

dataset = transformLabeledData()

documents = dataset["documents"]
labels = dataset["labels"]

yt = []
for i in labels:
    # xt.append(i[1])
    # twitter: positive
    if i == "Bullish" :
        yt.append(1)
    else:
        yt.append(0)

t = Tokenizer()
t.fit_on_texts(documents)
vocab_size = len(t.word_index) + 1
encoded_docs = t.texts_to_sequences(documents)
# print(encoded_docs)
max_length = 200
padded_docs = pad_sequences(encoded_docs, maxlen=max_length, padding='post')
# print(padded_docs)

embeddings_index = dict()
f = open('/Users/williamhenry/Downloads/glove.6B/glove.6B.200d.txt')
for line in f:
	values = line.split()
	word = values[0]
	coefs = asarray(values[1:], dtype='float32')
	embeddings_index[word] = coefs

f.close()
print('Loaded %s word vectors.' % len(embeddings_index))
# create a weight matrix for words in training docs
embedding_matrix = zeros((vocab_size, 200))
trainX, testX, trainY, testY = train_test_split( padded_docs, yt, test_size=0.3, random_state=1057) #42
# print (padded_docs)
for word, i in t.word_index.items():
	embedding_vector = embeddings_index.get(word)
	if embedding_vector is not None:
		embedding_matrix[i] = embedding_vector

# define model
model = Sequential()
e = Embedding(vocab_size, 200, weights=[embedding_matrix], input_length=200, trainable=False)
model.add(e)
# model.add(Dropout(0.2))
# model.add(Flatten())
model.add(LSTM(100, dropout=0.2, recurrent_dropout=0.2))
# model.add(Dense(32, activation='relu', kernel_regularizer=regularizers.l2(0.5)))
# model.add(Dropout(0.2))
model.add(Dense(1, activation='sigmoid'))
# compile the model
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])
# summarize the model
print(model.summary())
# fit the model
class_weight = {0:5, 1:3.65}
# X_resampled, y_resampled = SMOTE(ratio='auto', k_neighbors=5, kind='svm', random_state=1011).fit_sample(trainX, trainY)
model.fit(trainX, trainY, epochs=3, verbose=1, validation_split=0.1)

# evaluate the model
loss, accuracy = model.evaluate(testX, testY, verbose=1)
print('Accuracy: %f' % (accuracy*100))
print('Loss: %f' % (loss))

print ("Precision Score:")
pred1= model.predict_classes(testX)

print (precision_score(testY, pred1))
print ("Recall Score:")
print (recall_score(testY, pred1))
print ("F1 Score:")
print (f1_score(testY, pred1))
print ("Confusion Matrix:")
conf = confusion_matrix(testY,pred1)
print(conf)
tn, fp, fn, tp = conf.ravel()
print ((tn, fp, fn, tp))


