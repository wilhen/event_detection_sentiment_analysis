from StockPrice import getStockData
from pytz import timezone
import time
from datetime import datetime,timedelta

from pymongo import MongoClient
from DataUtils import setTempDateEvent
from pprint import pprint
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from textblob import TextBlob

# from Test import storeLeftOvers


eastern = timezone('US/Eastern')
utc = timezone('UTC')

# delete this
def getProcessedTweets(tweets, datas):
    temp = []
    dateEvent = []
    analyzer = SentimentIntensityAnalyzer()
    for i in tweets:
    # search for index for a given time
        present_date = i["created_at"].strftime('%Y-%m-%d')
        if present_date in datas:
            # present_trading = datas[j+1]

            # label = difference(datas[present_date]['1. open'],datas[present_date]['4. close'])
            # print(label)

            label =  analyzer.polarity_scores(i["text"])
            if label["compound"] >= 0.5:
                label = "positive"
            elif label["compound"] > -0.5 and label["compound"]  < 0.5:
                label = "neutral"
            elif label["compound"] <=-0.5:
                label = "negative"
                
            i["label"] = label
            temp.append(i)
            dateEvent.append((present_date,label))
    
    setTempDateEvent(dateEvent, 'dateEvent')

    return temp
# delete this
def processStockData(stockData, dataSet):
    
    time_series_data = stockData["Time Series (Daily)"]

    temp = dict()

    for i in dataSet:
        if i.strftime('%Y-%m-%d')  in time_series_data:
            temp[i.strftime('%Y-%m-%d') ] = time_series_data[i.strftime('%Y-%m-%d') ]

    return temp

def getTweets(symbol=None):

    client = MongoClient('localhost',27017)
    db = client.StockTwits
    # db = client.tweets
    # collection = db.djias #for djia only
    # collection = db.stream_gcp # all $AMZN
    if symbol == None:
        collection = db.MERGED_COLLECTION_2
    else:
        
        if symbol.lower() == "f":
            collection = db.F
        elif symbol.lower() == "ibm":
            collection = db.IBM
        elif symbol.lower() == "amzn":
            collection = db.AMZN
        elif symbol.lower() == "djia":
            collection = db.DJIA
        elif symbol.lower() == "ge":
            collection = db.GE
        elif symbol.lower() == "sq":
            collection = db.SQ
        elif symbol.lower() == "axp":
            collection = db.AXP
        elif symbol.lower() == "tsla":
            collection = db.TSLA
        elif symbol.lower() == "nke":
            collection = db.NKE
        elif symbol.lower() == "gpro":
            collection = db.GPRO
        elif symbol.lower() == "jnj":
            collection = db.JNJ
        elif symbol.lower() == "ntnx":
            collection = db.NTNX
        elif symbol.lower() == "sbux":
            collection = db.SBUX


    datasets = collection.find()
    noOfData = collection.count()
    client.close()

    return [list(datasets),noOfData]

def processStockTwits(datasets, number):
    temp = []

    if (len(datasets)!= number):
        raise Exception ("Unequal number of data!!!!")
    """
        Note: trial and error for proper dataset size
        StockTwits and DJIA are ~ 2000
        StockTwits: 2500
        DJIA: 2000
    """
    for i in datasets: 

        itemset = dict()

       
        itemset['id'] = i['id']
        createdAt = i["created_at"]

    
        itemset["text"] = i["message"]
    
        # Just in case 
       
        # format to datetime format
        createdAt = datetime.strptime(createdAt,'%Y-%m-%dT%H:%M:%SZ') #Fri Aug 04 08:12:40 +0000 2017
        # localise it to utc first
        utc_created_at = utc.localize(createdAt)
        # transform to EST
        est_created_at = utc_created_at.astimezone(eastern)
        est_created_at_date = est_created_at.date()
        itemset['created_at_date'] = time.mktime(createdAt.timetuple()) 
        itemset['created_at']= est_created_at_date
        
        itemset['yesterday_date']= est_created_at_date-timedelta(days=1)
        itemset['label']=i["sentiment"]
    

        temp.append(itemset)
    # storeLeftOvers(datasets[2500:])
    return temp

def processTweets(datasets, number, topic = False):

    temp = []
    if (len(datasets)!= number):
        raise Exception ("Unequal number of data!!!!")

    '''
        OLD: use 50000
        DJIA: use 10000
    '''
    test_date = set()
    for i in datasets[:101218]: 

        itemset = dict()

        # print(i["id"])
        itemset['id'] = i['id']
        createdAt = i["status"]["created_at"]


        if i["status"]["truncated"] == False:
            # original status and not truncated
           
            if(i['status']['lang']=="en"):
                if topic == False:
                    if TextBlob(i["status"]["text"]).sentiment.polarity !=0 or TextBlob(i["status"]["text"]).sentiment.subjectivity !=0:
                        itemset['text']= (i['status']['text'])
                       
                    else:
                        continue
                else:
                    itemset['text']= (i['status']['text'])
                
            else:
                continue
        else:
            # orginal status but truncated
           
            if(i['status']["lang"]=="en"):
                if topic == False:
                    if TextBlob(i["status"]['extended_tweet']['full_text']).sentiment.polarity !=0 or TextBlob(i["status"]['extended_tweet']['full_text']).sentiment.subjectivity !=0:
                        itemset['text']= (i['status']['extended_tweet']['full_text'])
                       
                    else:
                        
                        continue
                else:
                    itemset['text']= (i['status']['extended_tweet']['full_text'])
            else:
                continue
    
        # Just in case 
        # timestamp = i["status"]["timestamp_ms"]
        # format to datetime format
        createdAt = datetime.strptime(createdAt,'%a %b %d %H:%M:%S +0000 %Y')
        # localise it to utc first
        utc_created_at = utc.localize(createdAt)
        # transform to EST
        est_created_at = utc_created_at.astimezone(eastern)
        est_created_at_date = est_created_at.date()

        itemset['created_at']= est_created_at_date
        test_date.add(est_created_at_date)
        itemset['timestamp_ms'] = i['status']["timestamp_ms"]
        
        # itemset['yesterday_date']= est_created_at_date-timedelta(days=1)
        itemset['label']=None
        itemset["hashtags"] = i["status"]["entities"]["hashtags"]

        ts = int(itemset["timestamp_ms"])/1000

        dt = datetime.fromtimestamp(float(ts))
        dt_est = dt.astimezone(eastern)
        dt_est_date = dt_est.date()
       
        if dt_est_date.strftime('%Y-%m-%d') != "2017-09-07":
            continue
        else:
            """
                Time filtering 
                WHAATT!!!
                search bear: #timemanagement/WHATTT
            """
           
            temp.append(itemset)
       
    # print (test_date)
    return temp

# delete this one
def labeler(opening, closing, opening_criterion=0, closing_criterion=0):

    opening = float(opening)
    closing = float(closing)

    opening_criterion = float(opening_criterion)
    closing_criterion = float(closing_criterion)

    if (((closing-opening)/opening)- ((closing_criterion/opening_criterion)-1))> 0.002: #constant djia 0.005
        # print ("positive")
        return "positive"
    elif (((closing-opening)/opening) -((closing_criterion/opening_criterion)-1))< -0.002: #constant djia -0.004
        # print ("negative")
        return "negative"
    else:
        # print ("neutral")
        return "neutral"
#  delete this
def difference(opening,closing):

    opening = float(opening)
    closing = float(closing)

    return ((closing-opening)/opening)
# delete this
def setLabel(tweets, datas, criterion=None):

    temp = []
    
    for i in tweets:
        # search for index for a given time
        # for j in range(len(datas)):
        # yesterday_date = i["yesterday_date"].strftime('%Y-%m-%d')
        present_date = i["created_at"].strftime('%Y-%m-%d')
        if present_date in datas:
            # present_trading = datas[j+1]
            
            label = None
            if criterion!= None:
               
                # criterion = criterion["Time Series (Daily)"]
                label = labeler(datas[present_date]['1. open'],datas[present_date]['4. close'], criterion[present_date]['1. open'], criterion[present_date]['4. close'])
            elif criterion==None:
                label = labeler(datas[present_date]['1. open'],datas[present_date]['4. close'])
            
            # print (label)

            if label == "neutral":
               
                continue
            else:
                i["label"] = label
                print (i)
                # if label=="positive":
                #     print ("label: ", label)
                #     print (i)
                temp.append(i)
        
    return temp


def label(symbol):
   
    datas = getStockData()

    # criterion_prices = getCriterionPrice()
    # criterion_prices = criterion_prices["Time Series (Daily)"]
    # print(criterion_prices["2017-08-04"])
    if (type(datas)== Exception):
        raise datas
    elif datas==None:
        raise Exception('Transformation can not be completed')

    if symbol != None:
        (t,n) = getTweets(symbol)
    else:
        (t,n) = getTweets()

    tweets = processStockTwits(t,n)

    print("Stored in memory....")

    # dateSet = set()

    # for i in tweets:
    #     dateSet.add(i["created_at"])
        # dateSet.add(i["yesterday_date"])
    
    # datas = processStockData(datas,dateSet)
    
    # for twitter
    # tweets = getProcessedTweets(tweets, datas)
    # for twitter
    # tweets = setLabel(tweets,datas, criterion=criterion_prices) # uncomment this one to use the label

    return tweets

