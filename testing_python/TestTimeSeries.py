from pymongo import MongoClient
import pandas as pd
import numpy as np 
import json
from DatasetUtils import transformLabeledData
from datetime import datetime, timedelta
import time
import subprocess
from MapReducer import getDateID

def aggregateTotalDataHourly(symbol):

    dataset = transformLabeledData(symbol= symbol)
    documents = dataset["documents"]
    labels = dataset["labels"]
    ids = dataset["id"]
    timestamps = dataset["timestamps"]

    # print (timestamps)
    # Check same structure:

    # if len(documents) == len(labels) and len(labels) == len(ids) and len (ids) == len(timestamps):
    #     return True
    # else:
    #     return False

    start = min(timestamps)
    end = max(timestamps)
    """
        use this to transform timestamp to date time
        datetime.fromtimestamp(int(timestamp_int)).strftime('%Y-%m-%d %H:%M:%S')
    """
   
    endTuple = datetime.fromtimestamp(int(end))

    endDateHourObject = datetime.strptime(endTuple.strftime("%Y-%m-%d %H:00:00"), "%Y-%m-%d %H:00:00")
    endTupleDateHour = endDateHourObject.timetuple()
    endDateHour = time.mktime(endTupleDateHour)

    print(endTuple)
    print ()
    counter = 0
    store = dict()
    keys = dict()
    for i in range(len(timestamps)):

        if "$"+symbol.lower() not in documents[i]:
             continue
        # convert timestamp into a datetime object with complete datetime structure
        startTuple = datetime.fromtimestamp(int(timestamps[i]))

        # convert every date time object from timestamp to yyyy-mm-dd H:00:00 datetime object
        startDateHourObject = datetime.strptime(startTuple.strftime("%Y-%m-%d %H:00:00"), "%Y-%m-%d %H:00:00")
       
        # do conversion of datetime object to timetuple
        startTupleDateHour = startDateHourObject.timetuple()
        
        # timetuple to timestamp
        startDateHour = time.mktime(startTupleDateHour)

        # the next hour datetime object
        nextTuple = startDateHourObject+ timedelta(hours=1)

        # if startDateHour not in keys:
        #     print (startDateHour)
        #     counter+=1
        #     keys[startDateHour]=counter
            
        # else:
        #     counter = keys[startDateHour]
    
        if startDateHour <= timestamps[i] < time.mktime(nextTuple.timetuple()):
            # print (datetime.fromtimestamp(int(timestamps[i])).strftime('%Y-%m-%d %H:%M:%S'))
            # print (time.mktime(nextTuple.timetuple()))
            if startDateHour not in store:
                store[startDateHour] =1
            else:
                store[startDateHour] =  store[startDateHour] +1

        

    print()
    print(store)
   
    period1 = min(list(store.keys()))
    print (period1)
    print (store[period1])

    return store
  
def generateCSV(symbol):
    store = aggregateTotalDataHourly(symbol)
    listTuples = []
    for i in list(store.keys()):
        listTuples.append([i, store[i]])

    listTuples = np.array(listTuples)
    # print (listTuples[:,0])
    dataframe = pd.DataFrame(np.column_stack([listTuples[:,0], listTuples[:,1]]), columns=["Timestamp","Frequency"])
    dataframe.to_csv("./csv/StockTwits/StockTwits_"+symbol.upper()+"_Timeseries_Frequency.csv",sep=",",encoding='utf-8',index=False)


def callRScript(args):
    """
        Invoke the R process by subprocess API. Check the generated CSV files in the ./csv/output.
        Sort the timestamp column, get all tweets with the correct entity, combine them altogether 
        to make a dataset for the sentiment analysis.
    """
    
    subprocess.check_call("cd _r/; Rscript --vanilla AnomalyDetection.R "+ args.upper(), shell=True)
        
       

def main():
    symbols = ["DJIA","AMZN","GE","IBM", "F", "SQ","AXP","TSLA", "NKE" ,"GPRO", "JNJ" , "NTNX", "SBUX"]
    for sym in symbols:
        generateCSV(sym)
        callRScript(sym)

    getDatasetFromCSV(symbols)


def getDatasetFromCSV(syms):
    result=[]
    for sym in syms:

        dataframe = pd.read_csv("./csv/outputs/output_"+sym+".csv",usecols=[0,1])
        matrix = dataframe.as_matrix().tolist()
        # print (matrix)
        dateID = []
        eventIDs = {}
        for i in matrix:
            date = i[0] #datetime
            # convert every date time object from timestamp to yyyy-mm-dd H:00:00 datetime object
            startDateHourObject = datetime.strptime(date,"%Y-%m-%d %H:%M:%S")
            nextDateHour = startDateHourObject + timedelta(hours=1)
            dateID.append(getDateID(startDateHourObject, nextDateHour, sym))

        for i in range(len(dateID)):
            if i not in eventIDs:
                temp = []
                for obj in dateID[i]:   
                    temp.append(int(obj["_id"]))
                eventIDs[i] = temp

        client = MongoClient('10.152.0.3',27017)

        db = client.StockTwits
        symbol = sym
        # collection = db.MERGED_COLLECTION #changed to sym
        if symbol.lower() == "f":
            collection = db.F
        elif symbol.lower() == "ibm":
            collection = db.IBM
        elif symbol.lower() == "amzn":
            collection = db.AMZN
        elif symbol.lower() == "djia":
            collection = db.DJIA
        elif symbol.lower() == "ge":
            collection = db.GE
        elif symbol.lower() == "sq":
            collection = db.SQ
        elif symbol.lower() == "axp":
            collection = db.AXP
        elif symbol.lower() == "tsla":
            collection = db.TSLA
        elif symbol.lower() == "nke":
            collection = db.NKE
        elif symbol.lower() == "gpro":
            collection = db.GPRO
        elif symbol.lower() == "jnj":
            collection = db.JNJ
        elif symbol.lower() == "ntnx":
            collection = db.NTNX
        elif symbol.lower() == "sbux":
            collection = db.SBUX
        
    
        for g in list(eventIDs.keys()):
            for i in eventIDs[g]:
                col = collection.find_one({"id":i})
                
                result.append({"id":col["id"], "sentiment":col["sentiment"],"is_reshare_message":col["is_reshare_message"], "created_at": col["created_at"], "message":col["message"]})
    
    
    # print(eventIDs)
    with open('dataset.json', 'w') as outfile:
        json.dump(result, outfile)      

main()

# getDatasetFromCSV("GE")

