from TwitterData import label
from pymongo import MongoClient

import numpy as np
import pandas as pd 
import re

from sklearn.externals import joblib
import dill
from datetime import datetime

from TweetFiltering import n_grams_analyser
from nltk.util import ngrams
from DatasetUtils import transformLabeledData
from LSH import lsh_main
from datasketch import MinHash, MinHashLSH
import json
from sklearn.svm import OneClassSVM, SVC
from sklearn.utils import resample
from sklearn.metrics import precision_score, recall_score, accuracy_score, f1_score, silhouette_score, confusion_matrix
from sklearn.model_selection import train_test_split

from sklearn.ensemble import IsolationForest, RandomForestClassifier
from sklearn.neighbors import LocalOutlierFactor

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from imblearn.over_sampling import SMOTE

from sklearn.semi_supervised import label_propagation
from TfIdfVectorizer import analyser
from sklearn.decomposition import NMF, PCA
from sklearn.cluster import DBSCAN, KMeans
from sklearn.feature_extraction.text import TfidfVectorizer

from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_selection import SelectKBest, mutual_info_classif

def print_top_words(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        print("Topic #%d:" % topic_idx)
        print(" ".join([feature_names[i]
                        for i in topic.argsort()[:-n_top_words - 1:-1]]))
    print()


def create_csv(g_ids, r_text, n_labels):
    dataframe = pd.DataFrame(np.column_stack([g_ids[:], r_text[:], n_labels[:]]), columns=["id","text","label"])
    dataframe.to_csv("./data/outliers_dataset.csv",sep=",",encoding='utf-8',index=False)

client = MongoClient('10.152.0.3',27017)
db = client.stoxly
db2 = client.tweets
spams = db.spams
sentiment = db.sentiments

dates = "25/1/2018 26/1/2018 29/1/2018 30/1/2018 31/1/2018 1/2/2018 2/2/2018 5/2/2018 6/2/2018 7/2/2018 8/2/2018 9/2/2018 12/2/2018 13/2/2018 14/2/2018 15/2/2018 16/2/2018 20/2/2018 21/2/2018 22/2/2018 23/2/2018 26/2/2018 27/2/2018 28/2/2018 1/3/2018 2/3/2018 5/3/2018 6/3/2018 7/3/2018 8/3/2018"
# dates = "25/1/2018 26/1/2018"
dates = dates.split(" ")

print(dates)

_docs = []
_labels = []
_ids = []
_dates = []
r_docs = []

keys_fq = {}
minhashes = {}
keys_id = {}
t = 0.55
texts_ = []

lsh = MinHashLSH(threshold=t, num_perm=256 )
counter=0
for d in dates:
    date= datetime.strptime(d, "%d/%m/%Y").strftime("%Y-%m-%d")
    print (date)
    data = label("GE", date)
    
    dataset = transformLabeledData(dataset=data)
    documents = dataset["documents"]
  
    ids = dataset["id"]
    texts = dataset["texts"]

    for i in range(len(documents)):
        
        doc = documents[i]
        if len(doc) <3:
            continue

        n_grams = ngrams(doc,2)
        ndoc = [ '_'.join(grams) for grams in n_grams]
        if len(ndoc)>0:
           
            doc_set = set(ndoc)
        
            lsh, minhashes, keys_fq, keys_id = lsh_main(lsh, doc_set, ids[i], minhashes, keys_fq, keys_id, counter= counter,t=t)
            for ii in keys_id.keys():
                if ids[i] in keys_id[ii]:
                    
                    if keys_fq[ii] <=1:
                      
                        texts_.append(texts[i])
                        _docs.append(ndoc)
                        _ids.append(ids[i])
                       
                        _dates.append(date)
                        r_docs.append(doc)
        counter+=1

# print (keys_fq) 
# print (keys_id)

tfidf = joblib.load("./pickles/tfidf_GE_v0_08_04_2018.pkl")
vectors = tfidf.transform(_docs)

iso_forest = joblib.load("./pickles/iso_GE_v0_08_04_2018.pkl")
pred = iso_forest.predict(vectors)

v2 = vectors
vectors = vectors.toarray()

now = None
count = 0
count_norm = 0
n_labels = []

_vectors = []
_r_docs = []
# g_docs = []
g_vec = []
g_labels = []
g_ids = []
r_text = []

for i in range(len(pred)):

    spam = spams.find({"id":_ids[i], "spam":True})
    neutral = sentiment.find({"id":_ids[i], "sentiment":"neutral"})
    if now!= _dates[i]:
        now = _dates[i]
        print (now)

    _r_docs.append(r_docs[i])
    g_ids.append(_ids[i])
    r_text.append(texts_[i])

    if pred[i]<0:
       
        _vectors.append(vectors[i])
        n_labels.append(1)
        count+=1
    
    elif pred[i]>0 and spam.count()>0:
        
        # _r_docs.append(r_docs[i])
        _vectors.append(vectors[i])
        n_labels.append(1)
        count+=1

    elif pred[i]>0 and spam.count()==0:

        if neutral.count() >0:
            # _r_docs.append(r_docs[i])
            
            _vectors.append(vectors[i])
            n_labels.append(1)
            count+=1
        else:
            count_norm+=1
            # g_docs.append(r_docs[i])
            g_vec.append(vectors[i])
            n_labels.append(0)
        
print (count)
print (count_norm)

_vectors = np.array(_vectors)
n_labels = np.array(n_labels)

g_vec = np.array(g_vec)
g_labels = np.array(g_labels)

# n_labels = np.concatenate((n_labels, g_labels))

# vectorizer = joblib.load("./pickles/tfidf_vectorizer_x1_07_03_2018.pkl")
vectorizer = TfidfVectorizer(max_df=1.0, min_df=10, analyzer=lambda x:x ,ngram_range=(1, 2))


vectors = vectorizer.fit_transform(_r_docs)

tfidf_feature_names = vectorizer.get_feature_names()
# print (tfidf_feature_names)

tfidf = dict(zip(tfidf_feature_names, vectorizer.idf_))
# print (tfidf)
# print(vectors.shape)
g_ids = np.array(g_ids)

ch2 = SelectKBest(mutual_info_classif, k=200)
vectors = ch2.fit_transform(vectors, n_labels)


# n_topics = 20
# n_top_words = 10
# nmf  = NMF(n_components=n_topics, random_state=1,alpha=.1, l1_ratio=.5).fit(_vectors)

# print_top_words(nmf, tfidf_feature_names, n_top_words)
# print (nmf.components_)


# dbs = DBSCAN(eps=0.8, min_samples=10, metric="cosine", algorithm="auto", leaf_size=30, p=None, n_jobs=2)
# dbs.fit(v2)



"""
    Twitter inspection part
"""

tweets = db2.djias
col2 = []

ACTION_DICTIONARY = ["long","short","shorting","trim","trimed", 
    "purchase", "add", "buy","sell", "purchased", "bought","sold",
    "bulls","bears","bear","adding","added","buying","acquire",
    "good","bad","acquisition","acquired","trimming", "decrease", 
    "increase","decreased","increased","decreasing","increasing",
    "up","down","drop","dropped","dropping","stake","love","hate",
    "fall", "fallen", "felt","breakout","dump","dumping","dumped",
    "downstick","high","low","picking","catfishing","catfished",
    "catfish", "caught","load","loading","loaded","downgrade", 
    "downgrading","downgraded","positive","negative","neutral",
    "hold","holding","held","dumpster","trash","garbage","red",
    "green", "active","uptrend","bigger", "pop", "overvalue",
    "overvalued","undervalue","undervalued", "overweight",
    "weak","strong","bullish", "bearish","stop", "sink", "sank",
    "sinking" ,"pick","selling","bull","underweight",
    "picking", "picked" , "powerful","put", "gain", "gaining", "messy",
    "gained","devalue","devalued","reduction","shorted", "fraud", "kick","kicked","kicks"
    "top" ,"trouble","troubled", "troubles","call", "buyer", "buyers","burning"
    "mess", "messed", "poison","poisonous","burn","burned","fly", "flies","flying"                 
    "crack","cracking", "cracks","cracked","picks","devalues","overvalues",
    "undervalues","falls","catfishes","buys","sells","dumps","ups","downs","lows",
    "downgrades","upgrades","holds", "below" ,"above","picks","sinks","joy","reduce","reduces","joys","joyed"
    "trims","owns","own","owning","owned","puts","calls", "movers","mover","reds","greens"
    "adds","acquires","purchases","decreases","increases","hated","hates","loves","shorts","longs",
    "reprieve","reprives","reprieved","reprieving","dip","dips","dipping","dipped", "breakups","breakup",
    "upped","upping","brokeup", "bankrupt","bankrupts","crisis","bankruptcy","fear","feared", "break","broke","broken", 
    "breaks","fix","fixing","fixes","fixed", "hopes","hoped",
    "fearing","fears","happy","sad","fish","fished","fishes","fishing","optimist","optimistic","optimists", "pessimists",
    "pessimist","pessimistic", "miss","missing","missed","misses", "blow","out","blown","blew","blows", 
    "lower","higher","lowered","lowering","lowered","cut","cutting","cuts","better","failed","fail","failure","fails"
    "drops","loads","reloads", "holds","crash","crashed","crashing"]

for i in range(len(g_ids)):

    t_col = tweets.find_one({"id":g_ids[i]})
    truncated = t_col["status"]["truncated"]
    verified = t_col["status"]["user"]["verified"]
    friends = t_col["status"]["user"]["friends_count"]
    followers = t_col["status"]["user"]["followers_count"]
    digits = 0
    actions = 0
    if not truncated:
        text =  [ t.lower().strip() for t in t_col["status"]["text"].split(" ")]
        
        for t in text:
            if t in ACTION_DICTIONARY:
                actions+=1
            for c in t:
                if c.isdigit():
                    digits+=1
                    break
        
        cashtags = [ ii["text"] for ii in t_col["status"]["entities"]["symbols"]]
        hashtag = [ ii["text"] for ii in t_col["status"]["entities"]["hashtags"]]
        url = [ ii["url"] for ii in  t_col["status"]["entities"]["urls"]]
        mentions = [ ii["screen_name"] for ii in t_col["status"]["entities"]["user_mentions"]]
        
    else:
        text =  [ t.strip() for t in t_col["status"]["extended_tweet"]["full_text"].split(" ")]
        
        for t in text:
            if t in ACTION_DICTIONARY:
                actions+=1
            for c in t:
                if c.isdigit():
                    digits+=1
                    break
        
        cashtags = [ ii["text"] for ii in t_col["status"]["extended_tweet"]["entities"]["symbols"]]
        hashtag = [ ii["text"] for ii in t_col["status"]["extended_tweet"]["entities"]["hashtags"]]
        url = [ ii["url"] for ii in t_col["status"]["extended_tweet"]["entities"]["urls"]]
        mentions = [ ii["screen_name"] for ii in t_col["status"]["extended_tweet"]["entities"]["user_mentions"]]
    
    number_of_cashtag = len(cashtags)
    number_of_url = len(url)
    number_of_hashtag = len(hashtag)
    number_of_mention = len(mentions)

    col2.append(np.array([ int(truncated), int( verified), number_of_cashtag, number_of_url, number_of_hashtag, number_of_mention, digits]))
   
col2 = np.array(col2)
# dataframe = pd.DataFrame(np.column_stack([g_ids[:], col2[:,0], col2[:,1], col2[:,2], col2[:,3], col2[:,4], col2[:,5], col2[:,6] ,col2[:,7], n_labels[:]]), columns=["id","truncated", "verified", "number_of_cashtag", "number_of_url","number_of_hashtag", "number_of_mention", "digits", "actions", "label"])
# dataframe.to_csv("./data/more_outlier2.csv",sep=",",encoding='utf-8',index=False)

X_neg = []
X_pos = []

X_neg2 = []
X_pos2 = []

y_neg = []
y_pos = []

vectors = vectors.toarray()
for i in range(len(n_labels)):
    if n_labels[i] == 0:
        X_pos2.append(col2[i])
        X_pos.append(vectors[i])
        y_pos.append(n_labels[i])
    else:
        X_neg2.append(col2[i])
        X_neg.append(vectors[i])
        y_neg.append(n_labels[i])

X1 , y1 = resample(X_pos, y_pos, replace=False, n_samples = 1500, random_state=1000)
X2 , y2 = resample(X_pos2, y_pos, replace=False, n_samples = 1500, random_state=1000)

X = np.vstack((X1, X_neg))
y = np.concatenate((y1, y_neg))

X2 = np.vstack((X2, X_neg2))
y2 = np.concatenate((y2, y_neg))

X_train, X_test, y_train, y_test = train_test_split( X, y, test_size=0.3, random_state=42)
X_train2, X_test2, y_train2, y_test2 = train_test_split( X2, y2, test_size=0.3, random_state=42)

lda = LinearDiscriminantAnalysis(solver="svd")
lda.fit(X_train, y_train)

pred  = lda.predict(X_test)


# print (pred.tolist())
# print (y_test.tolist())
print(accuracy_score(y_test, pred))
print(precision_score(y_test, pred))
print(recall_score(y_test, pred))
print(f1_score(y_test, pred))
conf = confusion_matrix(y_test, pred)
print (conf)

tn, fp, fn, tp = conf.ravel()
print ((tn, fp, fn, tp))


rf = RandomForestClassifier(max_features = "auto",  min_samples_leaf=10, n_estimators=100,
        oob_score=True, bootstrap=True, random_state=1190, verbose=1)

rf.fit(X_train, y_train)

pred  = rf.predict(X_test)


# print (pred.tolist())
# print (y_test.tolist())
print(accuracy_score(y_test, pred))
print(precision_score(y_test, pred))
print(recall_score(y_test, pred))
print(f1_score(y_test, pred))
conf = confusion_matrix(y_test, pred)
print (conf)

tn, fp, fn, tp = conf.ravel()
print ((tn, fp, fn, tp))



lda = LinearDiscriminantAnalysis(solver="svd")
lda.fit(X_train2, y_train2)

pred  = lda.predict(X_test2)


print(accuracy_score(y_test2, pred))
print(precision_score(y_test2, pred))
print(recall_score(y_test2, pred))
print(f1_score(y_test2, pred))
conf = confusion_matrix(y_test2, pred)
print (conf)

tn, fp, fn, tp = conf.ravel()
print ((tn, fp, fn, tp))


rf = RandomForestClassifier(max_features = "auto",  min_samples_leaf=10, n_estimators=100,
        oob_score=True, bootstrap=True, random_state=1190, verbose=1)

rf.fit(X_train2, y_train2)

pred  = rf.predict(X_test2)

print(accuracy_score(y_test2, pred))
print(precision_score(y_test2, pred))
print(recall_score(y_test2, pred))
print(f1_score(y_test2, pred))
conf = confusion_matrix(y_test2, pred)
print (conf)

tn, fp, fn, tp = conf.ravel()
print ((tn, fp, fn, tp))
