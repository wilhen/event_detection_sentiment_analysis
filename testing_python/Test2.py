from sklearn import metrics
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.datasets import fetch_20newsgroups
from sklearn.preprocessing import Normalizer

from sklearn.decomposition import TruncatedSVD, PCA, NMF
from sklearn.manifold import TSNE

from sklearn.cluster import KMeans, DBSCAN, MeanShift, AgglomerativeClustering, Birch, MiniBatchKMeans
from sklearn.pipeline import make_pipeline

import logging
import numpy as np

# import matplotlib.pyplot as plt

from hdbscan import HDBSCAN
import seaborn as sns
from nltk.util import ngrams
# from TopicModelling import getAmazon
import DatasetUtils
categories = [
    'alt.atheism',
    'talk.religion.misc',
    'comp.graphics',
    'sci.space',
]

# print("Loading 20 newsgroups dataset for categories:")
# print(categories)

# dataset = fetch_20newsgroups(subset='all', categories=categories,
#                              shuffle=True, random_state=42)

# print("%d documents" % len(dataset.data))
# print("%d categories" % len(dataset.target_names))
# print()

# labels = dataset.target
# true_k = np.unique(labels).shape[0]

# print (true_k)

# vectorizer = TfidfVectorizer(max_df=0.95, max_features=200,
#                                  min_df=5)

def print_top_words(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        print("Topic #%d:" % topic_idx)
        print(" ".join([feature_names[i]
                        for i in topic.argsort()[:-n_top_words - 1:-1]]))
    print()

# leftovers = getAmazon()
dataset = DatasetUtils.transformLabeledData()

# print (dataset)
documents = dataset["documents"]
labels = dataset["labels"]

docs=[]
for doc in documents:
    n_grams = ngrams(doc,2)
    ndoc = [ '_'.join(grams) for grams in n_grams]
    docs.append(ndoc)

documents = docs

tfidf_vectorizer = TfidfVectorizer(max_df=0.95, min_df=5, max_features=200, analyzer=lambda x:x,ngram_range=(1, 3))

X = tfidf_vectorizer.fit_transform(documents)

print("n_samples: %d, n_features: %d" % X.shape)

# svd = TruncatedSVD(2)
n_topics = 20
n_top_words = 10
nmf  = NMF(n_components=n_topics, random_state=1,alpha=.1, l1_ratio=.5).fit(X)

print("\nTopics in NMF model:")
tfidf_feature_names = tfidf_vectorizer.get_feature_names()
print_top_words(nmf, tfidf_feature_names, n_top_words)

pca = PCA(n_components=4)

normalizer = Normalizer(copy=False)
# pca = make_pipeline(pca, normalizer)
# lsa = make_pipeline(svd, normalizer)
"""
    For displaying data
    use TSNE

"""
print (np.mean(X))
X = pca.fit_transform(X.toarray())
# print (labels)
explained_variance = pca.explained_variance_ratio_.sum()
print("Explained variance of the PCA step: {}%".format(
    int(explained_variance * 100)))
print("noise variance of the PCA step: {}%".format(
    int(pca.noise_variance_ * 100)))
print("Mean of the PCA step: {}%".format(
    float(pca.mean_.mean())))

cluster_size=6
km = KMeans(n_clusters=cluster_size, init='k-means++', max_iter=100000, n_init=10 ) 
"""
    Try K-means
    It's the winner with --6 clusters--

    Agglomerative with 4-5 clusters is stable (>0.5 Silhouette Coefficient)
    * 4 is better and more stable
    * 5 is good but fluctuative
    * 3 is lower than 4
"""


minikm = MiniBatchKMeans(n_clusters=cluster_size)
# dbs = DBSCAN(eps=0.1, metric="cosine",  min_samples=8)
dbs = HDBSCAN(algorithm="best", min_cluster_size=10, min_samples=10, alpha=1.)
aggl = AgglomerativeClustering(n_clusters=3, affinity="cosine", linkage='average')
birch = Birch(threshold=0.18,  branching_factor = 20)

km.fit(X)
minikm.fit(X)
dbs.fit(X)
# mean.fit(X)
aggl.fit(X)
birch.fit(X)
# dbs.condensed_tree_.plot(select_clusters=True, selection_palette=sns.color_palette('deep', 8) )
# print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels, km.labels_))
# print("Completeness: %0.3f" % metrics.completeness_score(labels, km.labels_))
# print("V-measure: %0.3f" % metrics.v_measure_score(labels, km.labels_))
# print("Adjusted Rand-Index: %.3f"
#       % metrics.adjusted_rand_score(labels, km.labels_))
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, km.labels_, sample_size=1000))
print("Calinski and Harabaz score: %0.3f"
      % metrics.calinski_harabaz_score(X, km.labels_))
print ()

print("Minibatch Kmeans")
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, minikm.labels_, sample_size=1000))
print("Calinski and Harabaz score: %0.3f"
      % metrics.calinski_harabaz_score(X, minikm.labels_))
print ()


print("Agglomerative analysis")
# print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels, aggl.labels_))
# print("Completeness: %0.3f" % metrics.completeness_score(labels, aggl.labels_))
# print("V-measure: %0.3f" % metrics.v_measure_score(labels, aggl.labels_))
# print("Adjusted Rand-Index: %.3f"
#       % metrics.adjusted_rand_score(labels, aggl.labels_))
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, aggl.labels_, sample_size=1000))
print("Calinski and Harabaz score: %0.3f"
      % metrics.calinski_harabaz_score(X, aggl.labels_))


print("Birch analysis")
# print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels, aggl.labels_))
# print("Completeness: %0.3f" % metrics.completeness_score(labels, aggl.labels_))
# print("V-measure: %0.3f" % metrics.v_measure_score(labels, aggl.labels_))
# print("Adjusted Rand-Index: %.3f"
#       % metrics.adjusted_rand_score(labels, aggl.labels_))
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, birch.labels_, sample_size=1000))
print("Calinski and Harabaz score: %0.3f"
      % metrics.calinski_harabaz_score(X, birch.labels_))

print()

print("HDBSCAN analysis")
# print (dbs.labels_.tolist())

for i in range(len(dbs.labels_)):
    if dbs.labels_[i]==-1:
        # print(documents[i])
        pass
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, dbs.labels_, sample_size=3000))
print("Calinski and Harabaz score: %0.3f"
      % metrics.calinski_harabaz_score(X, dbs.labels_))

print()

print("Top terms per cluster:")

original_space_centroids = pca.inverse_transform(km.cluster_centers_)
order_centroids = original_space_centroids.argsort()[:, ::-1]


terms = tfidf_vectorizer.get_feature_names()
for i in range(cluster_size):
    print("Cluster %d:" % i, end='')
    for ind in order_centroids[i, :10]:
        print(' %s' % terms[ind], end='')
    print()
print()


print("Top terms per cluster:")

original_space_centroids_birch = pca.inverse_transform(birch.subcluster_centers_)
order_centroids_birch = original_space_centroids_birch.argsort()[:, ::-1]

for i in range(len(birch.subcluster_labels_)):
    print("Cluster %d:" % i, end='')
    for ind in order_centroids_birch[i, :10]:
        print(' %s' % terms[ind], end='')
    print()
print()


print("Top terms per cluster:")

original_space_centroids_mini = pca.inverse_transform(minikm.cluster_centers_)
order_centroids_mini = original_space_centroids_mini.argsort()[:, ::-1]

for i in range(cluster_size):
    print("Cluster %d:" % i, end='')
    for ind in order_centroids_mini[i, :10]:
        print(' %s' % terms[ind], end='')
    print()
print()

# tsne = TSNE(n_components=2, perplexity=70, verbose=1, n_iter=500)
# X = tsne.fit_transform(X)
# print (tsne.kl_divergence_)

# for i in range(len(km.labels_)):
#     # print (X[i,0], X[i,1])
    
#     if km.labels_[i] == 0:
#         plt.scatter(X[i,0], X[i,1], c='r')
        
#     elif km.labels_[i]==1:
#         plt.scatter(X[i,0], X[i,1], c='g')
#     elif km.labels_[i]==2:
#         plt.scatter(X[i,0], X[i,1], c='b')
#     elif km.labels_[i]==3:
#         plt.scatter(X[i,0], X[i,1], c='y')
#     elif km.labels_[i]==4:
#         plt.scatter(X[i,0], X[i,1], c='c')
# plt.scatter(X[:,0], X[:,1], c='c')
# plt.show()

# for i in range(len(aggl.labels_)):
#     # print (X[i,0], X[i,1])
    
#     if aggl.labels_[i] == 0:
#         plt.scatter(X[i,0], X[i,1], c='r')
        
#     elif aggl.labels_[i]==1:
#         plt.scatter(X[i,0], X[i,1], c='g')
#     elif aggl.labels_[i]==2:
#         plt.scatter(X[i,0], X[i,1], c='b')
#     elif aggl.labels_[i]==3:
#         plt.scatter(X[i,0], X[i,1], c='y')
#     elif aggl.labels_[i]==4:
#         plt.scatter(X[i,0], X[i,1], c='c')
#     elif aggl.labels_[i]==5:
#         plt.scatter(X[i,0], X[i,1], c='k')
  
# plt.show()

# for i in range(len(birch.labels_)):
#     # print (X[i,0], X[i,1])
    
#     if birch.labels_[i] == 0:
#         plt.scatter(X[i,0], X[i,1], c='r')
        
#     elif birch.labels_[i]==1:
#         plt.scatter(X[i,0], X[i,1], c='g')
#     elif birch.labels_[i]==2:
#         plt.scatter(X[i,0], X[i,1], c='b')
#     elif birch.labels_[i]==3:
#         plt.scatter(X[i,0], X[i,1], c='y')
#     elif birch.labels_[i]==4:
#         plt.scatter(X[i,0], X[i,1], c='c')
#     elif birch.labels_[i]==5:
#         plt.scatter(X[i,0], X[i,1], c='k')
  
# plt.show()



