from datetime import datetime, timedelta
from pymongo import MongoClient
from SentimentAnalyse import analyse_sentiment
import json
import uuid
from TfIdfVectorizer import analyser
from TweetFiltering import n_grams_analyser
from OutlierDataset import vector_analyser

client = MongoClient('10.152.0.3',27017)

def capture_nineth_date():

    db = client.tweets
    collection = db.djias2_datetime
    now = datetime(2018,5,13,00,00,00,00)
    tomorrow = now+timedelta(days=1)
    nineth = now - timedelta(days=4)
    u = uuid.uuid4()
    symbol = "GE"
    symbols = [symbol, "MCD","IBM","MMM","WMT","XOM","CAT","F","GM","CVX","BA","AXP","NKE"]
    # symbols = [symbol]
    mapR = collection.find({"$and":[{"value":{"$gte":nineth}},{"value":{"$lt":tomorrow}}]})
    tweets = db.djias
    last_id = 2
    counter = 0
    for i in mapR:
        if counter %10 ==0:
            last_id+=1
        t = tweets.find_one({"id":i["_id"], "$or":[{"tickers.status":{"$in": symbols}},{"tickers.quoted_status":{"$in":symbols}} ]})
        if t == None:
            continue

        doc = dict()
        doc["status"] = t["status"]
        doc["id"] = t["id"]
        doc["tickers"] = t["tickers"]
        doc["uuid"] = str(last_id)
        doc["created_at"] = (i["value"]).strftime("%Y-%m-%d %H:%M:%S")
        doc = json.dumps(doc)
        doc = doc.encode("utf-8")
        analyse_sentiment(None, active=False, data=doc, forced=True)
        counter+=1
capture_nineth_date()
        
        

  