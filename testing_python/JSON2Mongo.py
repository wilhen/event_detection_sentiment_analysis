import json
from pymongo import MongoClient
from pprint import pprint

with open('/Users/williamhenry/Desktop/mongo_dump/mongo_dump_18_09_2017.json') as f:
    data = json.load(f)

print (len(data))

client = MongoClient('localhost',27017)

db = client.tweets
collection = db.stream_gcp

for i in data:
    obj = {}
    obj['id']= i['id']
    obj['status']= i['status']
    obj['tickers']=i['tickers']
    collection.insert(obj)
