from kafka import KafkaConsumer
from pymongo import MongoClient
from datetime import datetime
import json
import time
import urllib3
import redis
from SentimentAnalyse import run_background
from TfIdfVectorizer import analyser

from TweetFiltering import n_grams_analyser
from OutlierDataset import vector_analyser

consumer = KafkaConsumer('test', group_id='view', bootstrap_servers=['10.152.0.5:9092'])
client = MongoClient('10.152.0.3',27017)
db = client.BatchLog
logs = db.kafka_consumer_log
red = redis.StrictRedis(host='localhost', port=6379)
def errorNotification(e):

    webhook_url = "https://hooks.slack.com/services/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"
    http = urllib3.PoolManager()

    body = {"channel":"williamhenry", "username": "Stoxly", "text": str(e)}
    json_body  = json.dumps(body)
    r = http.request("POST", webhook_url, headers={'Content-Type': 'application/json'}, body=json_body )

def kafkastream():
    
    for msg in consumer:
        # print (msg.value)
        red.set('x', msg.value)
        
        if msg.value!= None and len(msg.value)>0:
            run_background(msg.value)
    
    
#try:
kafkastream()
"""
except Exception as e:
   errorNotification(e)
   dt = datetime.now()
   log = {"status":"Kafka error","message": str(e), "updated_at": dt}
   logs.insert(log)
"""
