from gensim.models import Doc2Vec
from sklearn.feature_selection import SelectFromModel, SelectKBest, f_classif, SelectFdr, SelectFwe
from sklearn.svm import SVC
from sklearn.metrics import precision_score, recall_score, confusion_matrix, f1_score, accuracy_score, roc_curve
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier, BaggingClassifier, AdaBoostClassifier
from sklearn.externals import joblib
from scipy.sparse import vstack

from sklearn.semi_supervised import LabelSpreading, LabelPropagation

from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split, cross_val_score, KFold

import redis
import json
import numpy as np
import pandas as pd

from sklearn.utils import class_weight
from imblearn.over_sampling import SMOTE, RandomOverSampler, ADASYN
from imblearn.under_sampling import RandomUnderSampler, ClusterCentroids

# from xgboost import XGBClassifier

def saveDataFrame():
    trainX, trainY, testX, testY = generateDataFrame()
    
    #TODO: save the results into redis as json

    print (trainX)

def generateDataFrame():
    r = redis.StrictRedis(host='localhost',port=6379)
    X = json.loads(r.get("X"))
    xt = []
    yt = []
    for i in X:
        xt.append(i[1])
        if i[2]> 0 :
            yt.append(1)
        else:
            yt.append(0)

    xt = np.array(xt)
    yt = np.array(yt)

    # Check for the minority classes
    # and make the minority classes presented as insignificant majority

    # split set into their own classes


    X_train, X_test, y_train, y_test = train_test_split( xt, yt, test_size=0.3, random_state=42)

    return X_train, y_train, X_test, y_test


def getModel():
    # model = Doc2Vec.load("~/Documents/testing_python/tweets_model_dumped2b.doc2vec")
    model = Doc2Vec.load('~/Documents/testing_python/tweets_djia_model_dumped.doc2vec')
    # print(model.most_similar(["amazon"]))
    # print(model.docvecs["893382507500109824"])
    return model

def getDataFromMemory():
    r = redis.StrictRedis(host='localhost', port=6379)
    trainX = np.array(json.loads(r.get('trainX1')))
    trainY = np.array(json.loads(r.get('trainY1')))
    testX = np.array(json.loads(r.get('testX1')))
    testY = np.array(json.loads(r.get('testY1')))

    return trainX, trainY, testX, testY

def getScore():

    trainX, trainY, testX, testY = generateDataFrame()
    kbest = SelectFwe(score_func=f_classif, alpha=0.05)
    trainX = kbest.fit_transform(trainX,trainY)
    testX = kbest.transform(testX)
    anova = f_classif(trainX[0:100],trainY[0:100])
    print(anova[0])
    print(np.mean(anova[0]))

    return anova

def classify(trainX = None, trainY=None, testX = None, testY= None, XM=None, YM=None, feature_names = None):

    # if (trainX == None and trainY ==None and testX== None and testY==None):
    #     trainX, trainY, testX, testY = generateDataFrame()
    
    #     kbest = SelectFwe(score_func=f_classif, alpha=0.05)
    #     trainX = kbest.fit_transform(trainX,trainY)
    #     testX = kbest.transform(testX)


    classifier = LogisticRegression(class_weight={0: 5, 1:3}) 
    
    classifier.fit(trainX,trainY)
    print("Logistic Regression")
    
    print ("Accuracy Score:")
    print (classifier.score(testX,testY))
    print ("Precision Score:")
    pred0= classifier.predict(testX)
    print (precision_score(testY, pred0))
    print ("Recall Score:")
    print (recall_score(testY, pred0))
    print ("F1 Score:")
    print (f1_score(testY, pred0))
    print ("Confusion Matrix:")
    conf = confusion_matrix(testY,pred0)
    print (conf)

    tn, fp, fn, tp = conf.ravel()
   
    print ((tn, fp, fn, tp))

    print ("roc curve")

    fpr, tpr, thresholds = roc_curve(testY, pred0)
    print (fpr, tpr, thresholds)

    print ("Cross Validation: ")

    # X matrix are sparse
    # Xm = trainX.tolist() + testX.tolist()
    

    Xm = vstack([trainX, testX])
    Ym = trainY.tolist() + testY.tolist()

    print (cross_val_score(classifier, Xm, Ym, cv=10))

    


    """
    # clf = SVC(C=800, kernel='rbf', gamma=0.05, random_state=687, cache_size=200) #TODO: AMZN
    ### DJIA
    clf = SVC(C=100000, kernel='rbf', gamma=0.25, random_state=1021, cache_size=200)
    ### StockTwits
    C = 100000 // ::5500::
    gamma = ::0.01::
    kernel = rbf
    random_state= 1107
    ### StockTwits (Newer)
    For 200 features, chi k=45
    C=4800
    gamma = 0.01
    kernel = rbf
    random_state=1107

    """
    ### StockTwits
    # from 5500
    # dan ternyata C=100, kernel=rbf, gamma=auto, and  class_weight={0: 5.5, 1:3}
    """
        beta 1. C=100, gamma=auto, class_weight={0: 5.5, 1:3}
        beta 2. C=100, gamma=auto, class_weight={0: 5.25, 1:3}
        beta 3. C=5500, gamma=auto, class_weight={0: 5.5, 1:3}
    """
    clf = SVC(C=100, kernel='rbf', gamma="auto", random_state=1107, cache_size=500, verbose=1, class_weight={0: 5.5, 1:3})
   
    # scores = cross_val_score(clf, trainX1, trainY1, cv=10)
    # print("Cross validation score")
    # print (scores.mean())

    # X_resampled, y_resampled = SMOTE(ratio='minority', k_neighbors=5, kind='svm', random_state=1011, n_jobs=2).fit_sample(trainX, trainY)
    clf.fit(trainX, trainY)
    pred = clf.predict(testX)
    print ("SVM classifier")
    print ("Accuracy Score:")
    print (clf.score(testX,testY))
    print ("Precision Score:")
    print (precision_score(testY, pred))
    print ("Recall Score:")
    print (recall_score(testY, pred))
    print ("F1 Score:")
    print (f1_score(testY, pred))
    print ("Confusion Matrix:")
    conf = confusion_matrix(testY,pred)
    print (conf)
    tn, fp, fn, tp = conf.ravel()
    print ((tn, fp, fn, tp))

    print ("roc curve")

    fpr, tpr, thresholds = roc_curve(testY, pred)
    print (fpr, tpr, thresholds)
    
    ### persist it
    # joblib.dump(clf,"./pickles/svm_x0_25_03_2018.pkl")
    ###### NEW ROC VALUATION BASED MODEL ######
    ######                               ######
    ######                               ######
    # joblib.dump(clf,"./pickles/ROC_based/roc_svm_beta3.pkl")
    ######                               ######

    """
        Let's try 10 fold CV
        use Xm and Ym
    """
    # kf  = KFold(n_splits=10)
    # clfk = SVC(C=100000, kernel='rbf', gamma=0.01, random_state=1107, cache_size=200)
    # for train_index, test_index in kf.split(XM):
    #     print("TRAIN:", train_index, "TEST:", test_index)
    #     X_ktrain, X_ktest = XM[train_index], XM[test_index]
    #     y_ktrain, y_ktest = YM[train_index], YM[test_index]
    #     clfk.fit(X_ktrain, y_ktrain)
    #     pred = clfk.predict(X_ktest)
    #     print ("SVM classifier")
    #     print ("Accuracy Score:")
    #     print (clfk.score(X_ktest,y_ktest))
    #     print ("Precision Score:")
    #     print (precision_score(y_ktest, pred))
    #     print ("Recall Score:")
    #     print (recall_score(y_ktest, pred))
    #     print ("F1 Score:")
    #     print (f1_score(y_ktest, pred))
    #     print ("Confusion Matrix:")
    #     conf = confusion_matrix(y_ktest,pred)
    #     print (conf)
    #     tn, fp, fn, tp = conf.ravel()
    #     print ((tn, fp, fn, tp))



    """
        Use this Cross Validation if the training should be better!!!! Otherwise, it is very slow!!!!
        print ("Cross Validation: ")
        print (cross_val_score(clf, Xm, Ym, cv=10))
    """

    """
        Uncomment the clf2 for random forrest classifier
    """

    print("Random Forrest Classifier")

    clf2 = RandomForestClassifier(n_estimators=100, criterion="entropy", max_features = "auto",  min_samples_leaf=20, 
        oob_score=True, bootstrap=True, random_state=1190, verbose=1, class_weight={0:6.6,1:3.})
    
    # X_resampled, y_resampled = SMOTE(ratio='auto', k_neighbors=5, kind='svm', random_state=1011, n_jobs=4).fit_sample(trainX, trainY)
    clf2.fit(trainX, trainY)

    
    print("Accuracy Score:")
    print(clf2.score(testX,testY))

    pred2 = clf2.predict(testX)
    print ("Precision Score:")
    print (precision_score(testY, pred2))
    print ("Recall Score:")
    print (recall_score(testY, pred2))
    print ("F1 Score:")
    print (f1_score(testY, pred2))
    print ("Confusion Matrix:")
    conf = confusion_matrix(testY,pred2)
    print (conf)
    tn, fp, fn, tp = conf.ravel()
    print ((tn, fp, fn, tp))
    print ("roc curve")

    fpr, tpr, thresholds = roc_curve(testY, pred2)
    print (fpr, tpr, thresholds)
    

    # print ("XGB Classifier")
    # xgb = XGBClassifier(max_depth=4, learning_rate=.05, n_estimators=100, gamma=0, reg_lambda=1,
    #     objective= 'binary:logistic', random_state=1187, nthread=2, min_child_weight=6, 
    #     subsample=.8, colsample_bytree=.8)

    # xgb.fit(X_resampled, y_resampled)

    # xpred = xgb.predict(testX)
   
    # print("Accuracy Score:")
    # print(accuracy_score(testY,xpred))

    
    # print ("Precision Score:")
    # print (precision_score(testY, xpred))
    # print ("Recall Score:")
    # print (recall_score(testY, xpred))
    # print ("F1 Score:")
    # print (f1_score(testY, xpred))
    # print ("Confusion Matrix:")
    # conf = confusion_matrix(testY,xpred)
    # print (conf)
    # tn, fp, fn, tp = conf.ravel()
    # print ((tn, fp, fn, tp))

# print(model.infer_vector(['tesla','good','buy']))
# classify()
