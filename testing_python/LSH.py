import logging
import numpy as np
import pandas as pd
from nltk.util import ngrams

# import matplotlib.pyplot as plt

import DatasetUtils

from datasketch import MinHash, MinHashLSH
from pprint import pprint
from TwitterData import label
from pathlib import Path
import pickle
import redis

r = redis.StrictRedis(host='localhost', port=6379)

def main():
    date = "2018-02-08"
    data = label("GE", date)
    dataset = DatasetUtils.transformLabeledData(dataset=data)

    documents = dataset["documents"]
    labels = dataset["labels"]
    # timestamps = dataset["timestamp_ms"]
    ids = dataset["id"]

    lsh_function(documents, labels, ids, date)

def lsh_function(documents, labels, ids, date):
    #take 5 documents first
    doc5 = documents
    
    filename = "/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/LSH/lsh-"+date+".pkl"
    kf = "/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/LSH/kf-"+date+".pkl"
    mh = "/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/LSH/mh-"+date+".pkl"
    ki = "/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/LSH/ki-"+date+".pkl"
    t= .55
    
    lsh = MinHashLSH(threshold=t, num_perm=256 )

    my_file = Path(filename)
    counter=0
    keys_fq = {}
    minhashes = {}
    keys_id = {}
    k = "m108.2018-02-09"

    
    if my_file.is_file():

        infile = open(filename,'rb')
        lsh = pickle.load(infile)
        infile.close()

        infile = open(kf,'rb')
        keys_fq = pickle.load(infile)
        infile.close()

        infile = open(mh,'rb')
        minhashes = pickle.load(infile)
        infile.close()

        infile = open(ki,'rb')
        keys_id = pickle.load(infile)
        infile.close()
        
        # continue last counter
        counter = int(r.get("lsh_counter"))
    

    # temp_text = {}

    # legend = [('m25',2), ('m37', 2), ('m51', 2), ('m56', 2), ('m69', 2), ('m71', 2), ('m72', 2), ('m73', 2), ('m76', 2), ('m77', 2), ('m79', 2), ('m82', 2), ('m99', 2), ('m110', 2), ('m119', 2), ('m130', 2), ('m134', 2), ('m180', 2), ('m186', 2), ('m190',2), ('m198', 2), ('m202', 2), ('m205', 2), ('m215', 2), ('m236', 2), ('m239', 2), ('m241', 2), ('m249', 2), ('m261', 2), ('m263', 2), ('m265', 2), ('m299', 2), ('m312', 2), ('m328', 2), ('m333', 2), ('m348', 2), ('m356', 2), ('m366', 2), ('m368', 2), ('m447', 2), ('m461', 2), ('m463', 2), ('m473', 2), ('m5', 1), ('m10', 1), ('m11', 1), ('m15', 1), ('m28', 1), ('m31', 1), ('m32', 1), ('m54', 1), ('m55', 1), ('m58', 1), ('m59', 1), ('m60', 1), ('m61', 1), ('m62', 1), ('m63', 1), ('m64', 1), ('m65', 1), ('m66', 1), ('m67', 1), ('m68', 1), ('m74', 1), ('m75', 1), ('m85', 1), ('m87', 1), ('m88', 1), ('m89',1), ('m90', 1), ('m92', 1), ('m95', 1), ('m96', 1), ('m97', 1), ('m98', 1), ('m100', 1), ('m101', 1), ('m113', 1), ('m114', 1), ('m116', 1), ('m117', 1), ('m118', 1), ('m122', 1), ('m123', 1), ('m124', 1), ('m125', 1), ('m126', 1), ('m127', 1), ('m128', 1), ('m129', 1), ('m132', 1), ('m133', 1), ('m135', 1), ('m136', 1), ('m139', 1), ('m140', 1), ('m143', 1), ('m144', 1), ('m145', 1), ('m146', 1), ('m147', 1), ('m151', 1), ('m153', 1), ('m154', 1), ('m158', 1), ('m159', 1), ('m160',1), ('m161', 1), ('m162', 1), ('m163', 1), ('m164', 1), ('m165', 1), ('m166', 1), ('m167', 1), ('m168', 1), ('m169', 1), ('m170', 1), ('m174', 1), ('m175', 1), ('m177', 1), ('m178', 1), ('m182', 1), ('m184', 1), ('m185', 1), ('m187', 1), ('m188', 1), ('m189', 1), ('m192', 1), ('m193', 1), ('m194', 1), ('m195', 1), ('m196', 1), ('m197', 1), ('m201', 1), ('m203', 1), ('m204', 1), ('m206', 1), ('m207', 1), ('m210', 1), ('m211', 1), ('m212', 1), ('m213', 1), ('m214', 1), ('m216', 1), ('m217', 1), ('m219', 1), ('m222', 1), ('m223', 1), ('m224', 1), ('m225', 1), ('m227', 1), ('m228', 1), ('m230', 1), ('m231', 1), ('m232', 1), ('m233', 1), ('m234', 1), ('m235', 1), ('m237', 1), ('m238', 1), ('m240', 1), ('m244', 1), ('m245', 1),('m247', 1), ('m248', 1), ('m250', 1), ('m253', 1), ('m254', 1), ('m256', 1), ('m257', 1), ('m258', 1), ('m259', 1), ('m260', 1), ('m262', 1), ('m268', 1), ('m272', 1), ('m274', 1), ('m276', 1), ('m278', 1), ('m279', 1), ('m280', 1), ('m282', 1), ('m283', 1), ('m286', 1), ('m287', 1), ('m288', 1), ('m291', 1), ('m295', 1), ('m296', 1), ('m298', 1), ('m300', 1), ('m302', 1), ('m303', 1), ('m304', 1), ('m306', 1), ('m307', 1), ('m308', 1), ('m309', 1), ('m310', 1), ('m311', 1), ('m314', 1), ('m315', 1), ('m316', 1), ('m317', 1), ('m319', 1), ('m320', 1), ('m321', 1), ('m323', 1), ('m330', 1), ('m331', 1), ('m334', 1), ('m335', 1), ('m336', 1), ('m337', 1), ('m338', 1), ('m339', 1), ('m340', 1), ('m342', 1), ('m344', 1), ('m345', 1), ('m346', 1), ('m347', 1), ('m349', 1), ('m353', 1), ('m354', 1), ('m355', 1), ('m358', 1), ('m361', 1), ('m364',1), ('m365', 1), ('m367', 1), ('m370', 1), ('m371', 1), ('m372', 1), ('m373', 1), ('m374', 1), ('m375', 1), ('m376', 1), ('m379', 1), ('m380', 1), ('m383', 1), ('m387', 1), ('m393', 1), ('m395', 1), ('m396', 1), ('m397', 1), ('m399', 1), ('m401', 1), ('m402', 1), ('m403', 1), ('m404', 1), ('m405', 1), ('m408', 1), ('m409', 1), ('m410', 1), ('m412', 1), ('m414', 1), ('m415', 1), ('m416', 1), ('m417', 1), ('m420', 1), ('m421', 1), ('m422', 1), ('m423', 1), ('m424', 1), ('m426', 1), ('m427', 1), ('m430', 1), ('m431', 1), ('m432', 1), ('m433', 1), ('m434', 1), ('m436', 1), ('m438', 1), ('m440', 1), ('m442', 1), ('m444', 1), ('m446', 1), ('m449', 1), ('m450', 1), ('m451', 1), ('m452', 1), ('m453', 1), ('m455', 1), ('m457', 1),('m465', 1), ('m466', 1), ('m468', 1), ('m472', 1), ('m478', 1), ('m480', 1), ('m482', 1), ('m485', 1), ('m486', 1), ('m487', 1), ('m488', 1)]

    # legend resolver
    # _legend = []
    # for i in legend:
    #     _legend.append(i[0])

    for i in range(len(doc5)):
        # doc is tokenized
        doc = doc5[i]
        
        if len(doc) < 3:
            continue
        
        n_grams = ngrams(doc,2)
        ndoc = [ ' '.join(grams) for grams in n_grams]
        doc_set = set(ndoc)
        m = MinHash(num_perm=256)

        if "m"+str(counter) == k:
            # temp_text["m"+str(counter)]= [doc_set]
            print (doc_set)

        for d in doc_set:
            
            m.update(d.encode("utf-8"))

        result = lsh.query(m)
        
        # print("Approximate neighbours with Jaccard similarity > "+str(t), result)
        # print("insert the minhash to the minhash lsh")
    
        if len(result)==0:
        
            lsh.insert("m"+str(counter)+"."+date,m)
            minhashes["m"+str(counter)+"."+date] = m
            keys_fq["m"+str(counter)+"."+date] = 1
            keys_id["m"+str(counter)+"."+date] = [ids[i]]
    
        else:
            temp = []
            for key in result:
                minhash = minhashes[key]
                if (m.jaccard(minhash)>= t):
                    temp.append((key, minhash.jaccard(m)))
                    
                # print ("jaccard similarity: ",minhash.jaccard(m))
        
            if len(temp)>0:
                temp = sorted(temp, key=lambda x: x[1],reverse=True)  # only count the maximum jaccard similarity
                for ii in temp:
                    key = ii[0] # sorted. the maximum will be the first one
                    minhash = minhashes[key] #get the existence minhash with the highest jaccard similarity
                    minhash.merge(m) #merge the set
                    lsh.remove(key) #update process (1)
                    lsh.insert(key, minhash) #update process (2)
                    minhashes[key] = minhash #update the local copy
                    keys_fq[key] = keys_fq[key] +1 #update the frequency

                    x = keys_id[key]
                    x.append(ids[i])
                    keys_id[key]=x



                    if key == k:
                        print (doc_set)
                        # x= temp_text[key]
                        # x.append(doc_set)
                        # temp_text[key]=x

        counter+=1

    # pickle it
    outfile = open(filename,'wb')
    pickle.dump(lsh,outfile)
    outfile.close()

    outfile = open(kf,'wb')
    pickle.dump(keys_fq,outfile)
    outfile.close()

    outfile = open(mh,'wb')
    pickle.dump(minhashes,outfile)
    outfile.close()

    outfile = open(ki,'wb')
    pickle.dump(keys_id,outfile)
    outfile.close()

    # store counter
    r.set("lsh_counter", counter)

    # analyse(keys_fq)
    return keys_id


def analyse(keys_fq):
    _keys_fq = []
    for i in keys_fq.keys():
        # if (keys_fq[i]<=2):
        _keys_fq.append((i, keys_fq[i]))

    _keys_fq = sorted(_keys_fq, key=lambda x: x[1], reverse=True) 
    print ()
    print(_keys_fq)
    print ()

    # for x in temp_text.keys():

    #     print (x)
    #     print ()
    #     for i in temp_text[x]:

    #         print (i)

    print ("filtered ", len(_keys_fq))
    print ("# buckets: ",len(keys_fq))

def lsh_main(lsh, doc_set, ids, minhashes, keys_fq, keys_id, counter, t=0.55, k=None):

    m = MinHash(num_perm=256)

    # if "m"+str(counter) == k:
        # temp_text["m"+str(counter)]= [doc_set]
        # print (doc_set)

    for d in doc_set:
        
        m.update(d.encode("utf-8"))

    result = lsh.query(m)
        
    # print("Approximate neighbours with Jaccard similarity > "+str(t), result)
    # print("insert the minhash to the minhash lsh")
    
    if len(result)==0:
        
        lsh.insert("m"+str(counter),m)
        minhashes["m"+str(counter)] = m
        keys_fq["m"+str(counter)] = 1
        # keys_id["m"+str(counter)+"."+date] = [ids[i]]
        keys_id["m"+str(counter)] = [ids]

    else:
        temp = []
        for key in result:
            minhash = minhashes[key]
            if (m.jaccard(minhash)>= t):
                temp.append((key, minhash.jaccard(m)))
                
            # print ("jaccard similarity: ",minhash.jaccard(m))
    
        if len(temp)>0:
            temp = sorted(temp, key=lambda x: x[1],reverse=True)  # only count the maximum jaccard similarity
            for ii in temp:
                
                key = ii[0] # sorted. the maximum will be the first one
                minhash = minhashes[key] #get the existence minhash with the highest jaccard similarity
                minhash.merge(m) #merge the set
                lsh.remove(key) #update process (1)
                lsh.insert(key, minhash) #update process (2)
                minhashes[key] = minhash #update the local copy
                keys_fq[key] = keys_fq[key] +1 #update the frequency

                x = keys_id[key]
                # x.append(ids[i])
                x.append(ids)
                keys_id[key]=x

    return lsh, minhashes, keys_fq, keys_id

                # if key == k:
                    # print (doc_set)

                    # x= temp_text[key]
                    # x.append(doc_set)
                    # temp_text[key]=x

    

# main()
