import urllib3
import json
import redis

def getStockData(ticker="djia"):
    
    red = redis.StrictRedis(host='localhost', port=6379)
    http = urllib3.PoolManager()

    stock = red.get('stock')
    if(stock==None):
        print ('Retrieve stock data from alphavantage')
        # outputsize=full for wide time series stock
        r = http.request("GET", "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="+ticker+"&apikey=FYGG61GJ33X09NFO&datatype=json")
    
        if (r.status==200):

            data = json.loads(r.data.decode("utf-8"))

            if data!=None:
                red.set("stock", json.dumps(data))
                red.expire('stock', 7200)
                return data
            else:
                return None
        else:
            return Exception('Urlib Error!!!!!')
    else:
        print ('Retrieve stock data from cache')
        return json.loads(stock)


def getCriterionPrice():
    
    red = redis.StrictRedis(host='localhost', port=6379)
    http = urllib3.PoolManager()

    stock = red.get('criterion')
    if(stock==None):
        print ('Retrieve criterion price data from alphavantage')
        r = http.request("GET", "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=spx&apikey=FYGG61GJ33X09NFO&datatype=json&outputsize=full")
    
        if (r.status==200):

            data = json.loads(r.data.decode("utf-8"))

            if data!=None:
                red.set("criterion", json.dumps(data))
                red.expire('criterion', 7200)
                return data
            else:
                return None
        else:
            return Exception('Urlib Error!!!!!')
    else:
        print ('Retrieve criterion data from cache')
        return json.loads(stock)

