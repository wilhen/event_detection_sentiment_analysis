from pymongo import MongoClient
from bson.code import Code

def getDateID(startDate, endDate, symbol=None):
    client = MongoClient('10.152.0.3',27017)

    db = client.StockTwits
    if symbol ==None:
        collection = db.date_id
    else:

        if symbol.lower() == "f":
            collection = db.date_id_f
        elif symbol.lower() == "ibm":
            collection = db.date_id_ibm
        elif symbol.lower() == "amzn":
            collection = db.date_id_amzn
        elif symbol.lower() == "djia":
            collection = db.date_id_djia
        elif symbol.lower() == "ge":
            collection = db.date_id_ge
        elif symbol.lower() == "sq":
            collection = db.date_id_sq
        elif symbol.lower() == "axp":
            collection = db.date_id_axp
        elif symbol.lower() == "tsla":
            collection = db.date_id_tsla
        elif symbol.lower() == "nke":
            collection = db.date_id_nke
        elif symbol.lower() == "gpro":
            collection = db.date_id_gpro
        elif symbol.lower() == "jnj":
            collection = db.date_id_jnj
        elif symbol.lower() == "ntnx":
            collection = db.date_id_ntnx
        elif symbol.lower() == "sbux":
            collection = db.date_id_sbux

    query = collection.aggregate([{"$match":{"value":{"$gte":startDate.strftime("%Y-%m-%dT%H:%M:%SZ"), "$lt":endDate.strftime("%Y-%m-%dT%H:%M:%SZ")}}}])
    # print (list(query))
    return list(query)
    
def mapReduce(symbol):
    client = MongoClient('10.152.0.3',27017)

    db = client.StockTwits
    if symbol.lower() == "f":
        collection = db.F
    elif symbol.lower() == "ibm":
        collection = db.IBM
    elif symbol.lower() == "amzn":
        collection = db.AMZN
    elif symbol.lower() == "djia":
        collection = db.DJIA
    elif symbol.lower() == "ge":
        collection = db.GE
    elif symbol.lower() == "sq":
        collection = db.SQ
    elif symbol.lower() == "axp":
        collection = db.AXP
    elif symbol.lower() == "tsla":
        collection = db.TSLA
    elif symbol.lower() == "nke":
        collection = db.NKE
    elif symbol.lower() == "gpro":
        collection = db.GPRO
    elif symbol.lower() == "jnj":
        collection = db.JNJ
    elif symbol.lower() == "ntnx":
        collection = db.NTNX
    elif symbol.lower() == "sbux":
        collection = db.SBUX
    map = Code("function(){emit(this.id, this.created_at)}")
    reduce = Code("function(key, values){ return new Date(value);}")
    collection.map_reduce(map, reduce, "date_id_"+symbol.lower())
# getDateID("2018-01-19T20:00:00Z", "2018-01-20T20:00:00Z")


def main():

    symbols = ["GE","IBM", "F", "SQ","TSLA", "NKE" ,"GPRO", "JNJ" , "NTNX", "SBUX"]
    for sym in symbols:
        mapReduce(sym)
main()



