from pytz import timezone
import time
from datetime import datetime,timedelta

from pymongo import MongoClient, ASCENDING

from pprint import pprint
from textblob import TextBlob


eastern = timezone('US/Eastern')
utc = timezone('UTC')


def getTweets(symbol=None):

    client = MongoClient('10.152.0.3',27017)
    db = client.tweets
  
    collection = db.djias

    if symbol!=None:
        # datasets = collection.find({"$or":[{"tickers.status":{"$in":[symbol]}}, {"tickers.retweeted_status":{"$in":[symbol]}},{"tickers.quoted_status":{"$in":[symbol]}} ]})
        datasets = collection.find({"$or":[{"tickers.status":{"$in":[symbol]}},{"tickers.quoted_status":{"$in":[symbol]}} ]})
        # datasets = collection.find({"tickers.status":{"$in":[symbol]}})
        #.sort([("tickers.timestamp_ms", ASCENDING)])
    else:
        datasets = collection.find()

    client.close()

    return datasets

def processTweets(datasets, date, topic = False):

    temp = []

    '''
        OLD: use 50000
        DJIA: use 10000
    '''
    test_date = []
    for i in datasets: 

        itemset = dict()
      
        itemset['id'] = i['id']
        createdAt = i["status"]["created_at"]
        passes = False
        for user in i["status"]["entities"]["user_mentions"]:
            if user["screen_name"] == "realsheepwolf" or user["screen_name"]=="philstockworld":
                passes=True
        if passes:
            continue

        if i["status"]["truncated"] == False:
            # original status and not truncated
           
            if(i['status']['lang']=="en"):
          
                itemset['text']= (i['status']['text'])

            else:
                continue
        else:
            # orginal status but truncated
           
            if(i['status']["lang"]=="en"):
              
                itemset['text']= (i['status']['extended_tweet']['full_text'])
            else:
                continue
    
        # Just in case 
        # timestamp = i["status"]["timestamp_ms"]
        # format to datetime format
        createdAt = datetime.strptime(createdAt,'%a %b %d %H:%M:%S +0000 %Y')
        # localise it to utc first
        utc_created_at = utc.localize(createdAt)
        # transform to EST
        est_created_at = utc_created_at.astimezone(eastern)
        est_created_at_date = est_created_at.date()

        itemset['created_at']= est_created_at_date
        itemset["created_at_date"] = est_created_at_date
        itemset['timestamp_ms'] = i['status']["timestamp_ms"]
        test_date.append(itemset["timestamp_ms"])
        
        # itemset['yesterday_date']= est_created_at_date-timedelta(days=1)
      
        itemset["hashtags"] = i["status"]["entities"]["hashtags"]
        itemset["label"] = None
        ts = int(itemset["timestamp_ms"])/1000

        dt = datetime.fromtimestamp(float(ts))
        dt_est = dt.astimezone(eastern)
        dt_est_date = dt_est.date()

        if date == None:
            temp.append(itemset)
        else:
            if dt_est_date.strftime('%Y-%m-%d') != date:
                continue
            else:
                """
                    Time filtering 
                    WHAATT!!!
                    search bear: #timemanagement/WHATTT
                """
                temp.append(itemset)
        
        # if "@realsheepwolf" in itemset["text"]:
        #     print (itemset["text"])
        # temp.append(itemset) # dont' append all!!
       
    # print("min ", str(min(test_date)))
    # start = float( int(min(test_date))/1000)
    # end = float( int(max(test_date))/1000)
    # print(datetime.fromtimestamp(start).astimezone(eastern).date())
    # print("max ", str(max(test_date)))
    # print(datetime.fromtimestamp(end).astimezone(eastern).date())

    # test_date_set = set(test_date)
    # test_date_set = list(test_date_set)
    # test_date_set.sort()
    # for t in test_date_set:
    #     ts = float( int(t)/1000)
    #     print(datetime.fromtimestamp(ts).astimezone(eastern).date())


    return temp

def label(symbol, date, data=None):
    
    if data == None:
        if symbol != None:
            t = getTweets(symbol)
        else:
            t = getTweets()
    else:
        t = data

    tweets = processTweets(t, date=date)
   
    return tweets

# label("GE")
