from sklearn.feature_selection import SelectKBest, chi2, mutual_info_classif
from sklearn.externals import joblib
def extractBestFeature(feature_names, X, Y):

    """
        usually 75/80
        StockTwits (#2500) == 30
        StockTwits (200 Features) ==45
        ----------------------------------------------
        Topic Based with K-means (4 Clusters) == 100 

    """
    ch2 = SelectKBest(mutual_info_classif, k=600)
    X = ch2.fit_transform(X, Y)

    # joblib.dump(ch2,"./pickles/chi2Best_x0_14_03_2018.pkl")

    # print ("support")
    # print (ch2.get_support(indices=False))
    if feature_names:
        # keep selected feature names
        feature_names = [feature_names[i] for i
                         in ch2.get_support(indices=True)]
        print(feature_names)
    # print("done in %fs" % (time() - t0))
    return X, Y, feature_names