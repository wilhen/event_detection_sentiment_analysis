from pprint import pprint

import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor
from sklearn.model_selection import train_test_split, KFold
from sklearn.svm import SVC, SVR

from sklearn.pipeline import Pipeline
from sklearn.metrics import mean_squared_error
# from matplotlib import pyplot
# from matplotlib.dates import date2num, datestr2num, DateFormatter, DayLocator

from datetime import datetime


df = pd.read_csv("./csv/Formula/stock_formula_GE2.csv", usecols=[0,1,2])

matrix = df.as_matrix().tolist()
matrix = np.array(matrix)

dates = matrix[:,0]
dates_ = []
for i in dates:
    dates_.append(datetime.strptime(i,"%Y-%m-%d"))
dates = np.array(dates_)

X = matrix[:,1]
y = matrix[:,2]
X =  np.array(list(map(lambda x: float(x),X)))
y =  np.array(list(map(lambda x: float(x),y)))
X = X.reshape(-1,1)

# trainX, testX, trainY, testY = train_test_split( X, y, test_size=0.2, random_state=1057) #42
# pprint (matrix)
# amzn = 33
# GE = 27
trainX = X[0:27]
trainY = y[0:27]
testX = X[27:]
testY = y[27:]

f_dates = dates[27:]

lin = LinearRegression()
lin.fit(trainX,trainY)

print (lin.score(testX,testY))
pred = lin.predict(testX)
print (mean_squared_error(testY,pred))
print ()

ridge = Ridge(alpha=2, solver= "saga",random_state=17 )
ridge.fit(trainX, trainY)
print (ridge.score(testX, testY))
pred = ridge.predict(testX)
print (mean_squared_error(testY,pred))
print ()

"""
    AMZN
    SVR(C=1, kernel='linear',  cache_size=200, epsilon=1.755) 
    GE
    SVR(C=1, kernel='linear',  cache_size=200, epsilon=0.01) 
    SVR(C=100, kernel='poly',  degree=2, cache_size=200, epsilon=0.01) 
"""

clf = SVR(C=55, kernel='linear', degree=2, cache_size=200, epsilon=0.01) 
clf.fit(trainX, trainY)
print (clf.score(testX, testY))
pred = clf.predict(testX)
print (mean_squared_error(testY,pred))

print ()
# This one is bad :(
# regr = RandomForestRegressor(n_estimators =117, criterion="mse",  min_samples_leaf=2, bootstrap = True, oob_score= True, random_state=12)
# regr.fit(trainX, trainY)
# print (regr.score(testX, testY))

"""
    AMZN = random_state=313
    GE= random_state=36 (epsilon=0.05)
    GE= random_state=115 (epsilon=0.01)
    GE= random_state=45 (C=100, kernel=poly, degree=2)
    -------------------------------------------------------------
    new GE: random_state = 7
    but amzn breaks at 7
    so SVR C decrease upto 55 and adaboost should have at least (random=5)
    

"""


i = 5
# while True:
print (i)
ada = AdaBoostRegressor(base_estimator=clf, n_estimators=100, learning_rate=1, random_state=i)
ada.fit(trainX, trainY)

pred = ada.predict(testX)
print (pred.tolist())

    
    
score = ada.score(testX, testY)
print (score) 
print (mean_squared_error(testY,pred))

"""
dates = date2num(f_dates)
pyplot.gca().xaxis.set_major_formatter(DateFormatter('%m/%d/%Y'))
pyplot.gca().xaxis.set_major_locator(DayLocator())
pyplot.plot(dates, testY,label="actual")
pyplot.plot( dates,pred, color='red',label="predicted")
pyplot.legend(loc='upper right')
pyplot.gcf().autofmt_xdate()
pyplot.show()
"""
    # if score>0.6:
    #     break
    # else:
    #     i+=1
