import dill
from sklearn.externals import joblib
import os, sys, getopt
import redis
import numpy as np

red = redis.StrictRedis(host='localhost', port=6379)
def predict(X, symbol):
    ada = joblib.load("/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/Regression/ada_regression_return_beta1.pkl")
    X = np.array([[X]])
    pred = ada.predict(X)
    print(pred[0])
    red.set(symbol+"_regress", pred[0])
    red.expire(symbol+"_regress", 300)


def main(argv):
    symbol = ""
    X = 0
    try:
        opts, args = getopt.getopt(argv,"hi:u",["score=", "symbol="])
    except getopt.GetoptError:
        print ('RegressionModelAbstraction.py --symbol=<symbol> --score=<score>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print ('RegressionModelAbstraction.py --symbol=<symbol> --score=<score>')
            sys.exit()
        
        elif opt in ("-score", "--score"):
            score = arg
        elif opt in ("-symbol", "--symbol"):
            symbol = arg
        
    print ('symbol', symbol)
    print ("score", float(score.strip()))
    symbol = symbol.strip()
    X = float(score.strip())
    predict(X, symbol)


if __name__ == "__main__":
    main(sys.argv[1:])
    

