from nltk.tokenize import TweetTokenizer
from nltk.stem.wordnet import WordNetLemmatizer
from krovetzstemmer import Stemmer
from nltk.corpus import stopwords
from nltk import pos_tag
import re
from nltk.corpus import sentiwordnet as swn


from pymongo import MongoClient
client = MongoClient('localhost',27017)

tokenizer = TweetTokenizer()
# wordnet = WordNetLemmatizer()
stem = Stemmer()

def tokenize(tweet):
    try:

        # remove all stock tickers
        # tweet = re.sub("[$](([A-Za-z]))\w+", "STOCK_TICKER", tweet) #old regex

        # more reliable from (https://github.com/adonoho/TweetTokenizers/blob/master/PottsTweetTokenizer.py)
        tweet = re.sub("(?:\$[a-zA-Z]{1,6}([._][a-zA-Z]{1,2})?)", "STOCK_TICKER", tweet) #new one

        cashtag_rule = "(?:\$[a-zA-Z]{1,6}([._][a-zA-Z]{1,2})?)"
        # cashtag = re.compile(cashtag_rule)
        cashtags = []
        for m in re.finditer(cashtag_rule, tweet):
            # doted symbol
            cashtags.append((m.start(), m.end(), m.group(0), m.group(0)[1:]))
            if "." in m.group(0):
                sym = m.group(0).upper().replace(".", "_DOT_")
                tweet = tweet.replace(m.group(0), sym + "_TICKER")
            else:
                tweet = tweet.replace(m.group(0), m.group(0).upper() + "_TICKER")
        
        stop = set(stopwords.words('english'))
        
        tokens = tokenizer.tokenize(tweet.encode("UTF-8").lower())
        tokens = pos_tag(tokens)

        # Should do a POS Tagging before removals
        # In addition, do a lemmatiation or stemming based on the POS Tags
        # Do removals
        tokens = filter(lambda t: not (t[0]).startswith('@'), tokens)
        """
            Uncomment the below filter to avoid hashtags
        """
        tokens = filter(lambda t: not (t[0]).startswith('#'), tokens)
        tokens = filter(lambda t: not (t[0]).startswith('http'), tokens)
        tokens = filter(lambda t: t[0]!="rt", tokens)
        tokens = filter(lambda t: t[0]!="stock_ticker", tokens) # not used anymore, since we need the stock symbols
        tokens = filter(lambda t: not t[0].isdigit(), tokens) # remove any number
        temp = []

        for i in tokens:
            if i[0] not in stop:
                temp.append(i)
           
        pattern = re.compile('[^\w]')
        hashtag = re.compile("\B#\w*[a-zA-Z]+\w*")
        cashtag_part = re.compile("\w+_ticker")
        preProcessed = []

        for i in temp:
            if (pattern.match(i[0])==None):
                """
                    @williamhenry: delete this one if you don't want to sort vocab
                """
                # if ((i[1]).startswith("VB") or (i[1]).startswith("NN")):
               
                """
                    You may need to reconstruct the stock symbol here.
                    Firstly, you need to check if the string contains a random symbols after tokenization
                    and filtering. Remove all random symbols. The string with symbol_ticker will not be filtered
                    Secondly, we check if a word match with the patter symbol_ticker or not. if not, just append it
                    if yes, find the word in that cashtags dictionary of the document. 

                    WATCHOUT: for _dot_ pattern
                """
                if (cashtag_part.match(i[0])!=None):
                    sym = None
                    if "_dot_" in i[0]:
                        # if "_dot_" pattern exists
                        # replace it with real "."
                        sym = (i[0]).lower().replace("_dot_",".")
                    else:
                        # if not "_dot_" pattern, leave it that way
                        sym = i[0]

                    c = list(filter(lambda t: (t[3]+"_TICKER").lower() == sym.lower() , cashtags))
                    if (len(c)>0):
                        # if the symbol match with the one in the dictionary 
                        # append if
                        preProcessed.append(stem.stem(c[0][2]))
                else:
                    preProcessed.append(stem.stem(i[0]))

                """
                    This part is uncommented if we would like to add hashtag
                """

            # elif (hashtag.match(i[0])!=None):
            #     if ((i[1]).startswith("VB") or (i[1]).startswith("NN")):
            #         preProcessed.append(i[0])
                # preProcessed.append(wordnet.lemmatize(i))
        
        return preProcessed

    except:
        return "NC"


def main():
    
    tokens = []
    # polarities = []
    db = client.tweets
    collection = db.djias

    for i in collection.find():
        # print(i["id"])
        # print(i["status"]["text"])
       
        token = tokenize(i["status"]["text"])
        # print(token)
        tokens.append(token)
        # polarities.append(polarity)

    print(tokens)
    total_words = sum([ len (i) for i in tokens])
    print ("total words: "+ str(total_words))

# main()
