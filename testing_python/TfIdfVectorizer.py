from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from DatasetUtils import transformLabeledData
from sklearn.decomposition import LatentDirichletAllocation, NMF
from FeatureSelector import extractBestFeature
from sklearn.model_selection import train_test_split
import pandas as pd 
import numpy as np
from TesterModel import classify
import dill
from pymongo import MongoClient
from LabelData import processTweets

from sklearn.externals import joblib
from nltk.util import ngrams
no_features = 400


def wordEmbedding():
    with open("/Users/williamhenry/Downloads/glove.6B/glove.6B.200d.txt", "rb") as lines:
        w2v = {line.split()[0]: np.array(map(float, line.split()[1:]))
            for line in lines}

    embeddings_index = w2v
    return embeddings_index

def analyser(x):
    return x

def tfidf():

    dataset = transformLabeledData()
    # docs = []
    documents = dataset["documents"]
    # for doc in documents:
    #     n_grams = ngrams(doc,2)
    #     ndoc = [ '_'.join(grams) for grams in n_grams]
    #     docs.append(ndoc)
    # documents = docs
    labels = dataset["labels"]

    # print (documents)
   
    # min df: 20 (default 10)
    # or 50

    """
        IMPORTANT TO SAVE MODEL
        analyzer cannot be a lambda which is STUPID
        so let's create a function for this analyzer (call)
    """
    
    tfidf_vectorizer = TfidfVectorizer(max_df=1.0, min_df=10, analyzer=analyser,ngram_range=(1, 2))
    vectors = tfidf_vectorizer.fit_transform(documents)

    tfidf_feature_names = tfidf_vectorizer.get_feature_names()

    # print (len(tfidf_vectorizer.idf_))
    # print(tfidf_vectorizer.idf_)

    tfidf = dict(zip(tfidf_feature_names, tfidf_vectorizer.idf_))
    print ("vocab size : ", len(tfidf))
    # print(vectors)


    df = pd.DataFrame(columns=['tfidf']).from_dict(tfidf, orient='index')
    df.columns = ['tfidf']

    # print (df.sort_values(by=['tfidf'], ascending=False).head(30))
    # print (vectors.shape)
    
    yt = []
    for i in labels:
        # xt.append(i[1])
        # twitter: positive
        if i == "Bullish" :
            yt.append(1)
        else:
            yt.append(0)

   
    yt = np.array(yt)
    # xt=vectors
    xt, yt, feature_names = extractBestFeature(tfidf_feature_names, vectors, yt)

    # joblib.dump(tfidf_vectorizer,"./pickles/tfidf_vectorizer_x0_14_03_2018.pkl")

    """
        Note:
        DJIA = 1057
        AMZN = 42
        StockTwits = 1057
    """
    trainX, testX, trainY, testY = train_test_split( xt, yt, test_size=0.3, random_state=1057) #42

    # trainX = np.array(trainX)
    # trainY = np.array(trainY)
    # testX = np.array(testX)
    # testY = np.array(testY)
    a0 = 0
    a1 = 0
    for i in trainY:
        if i == 0:
            a0+=1
        else:
            a1+=1
    print (a0)
    print (a1)
    # classify(trainX, trainY, testX, testY, XM=xt, YM=yt) #uncomment this

# tfidf()