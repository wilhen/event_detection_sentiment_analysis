from TwitterData import label
from pymongo import MongoClient

import numpy as np
import pandas as pd 
import re

from sklearn.externals import joblib
import dill
from datetime import datetime

from TweetFiltering import n_grams_analyser
from nltk.util import ngrams
from DatasetUtils import transformLabeledData
from LSH import lsh_main
from datasketch import MinHash, MinHashLSH
import json
from sklearn.svm import OneClassSVM, SVC
from sklearn.utils import resample
from sklearn.metrics import precision_score, recall_score, accuracy_score, f1_score, silhouette_score, confusion_matrix
from sklearn.model_selection import train_test_split, KFold

from sklearn.cross_validation import cross_val_score

from sklearn.ensemble import IsolationForest, RandomForestClassifier, AdaBoostClassifier
from sklearn.neighbors import LocalOutlierFactor

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from imblearn.over_sampling import SMOTE

from TfIdfVectorizer import analyser
from sklearn.decomposition import NMF, PCA, TruncatedSVD
from sklearn.cluster import DBSCAN, KMeans
from sklearn.feature_extraction.text import TfidfVectorizer

from sklearn.feature_selection import SelectKBest, mutual_info_classif

client = MongoClient('10.152.0.3',27017)
db = client.stoxly
db2 = client.tweets

def outlier_tokenizer(text):
    tokens = text.split(" ")
    tokens = filter(lambda t: not t.startswith('http'), tokens)
    tokens = filter(lambda t: not t.startswith('@'), tokens)
    return tokens


def vector_analyser(x):
    return x

def print_top_words(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        print("Topic #%d:" % topic_idx)
        print(" ".join([feature_names[i]
                        for i in topic.argsort()[:-n_top_words - 1:-1]]))
    print()


def create_csv(g_ids, r_text, n_labels):
    dataframe = pd.DataFrame(np.column_stack([g_ids[:], r_text[:], n_labels[:]]), columns=["id","text","label"])
    dataframe.to_csv("./data/outliers_dataset.csv",sep=",",encoding='utf-8',index=False)

def data():

    
    spams = db.spams
    sentiment = db.sentiments

    dates = "25/1/2018 26/1/2018 29/1/2018 30/1/2018 31/1/2018 1/2/2018 2/2/2018 5/2/2018 6/2/2018 7/2/2018 8/2/2018 9/2/2018 12/2/2018 13/2/2018 14/2/2018 15/2/2018 16/2/2018 20/2/2018 21/2/2018 22/2/2018 23/2/2018 26/2/2018 27/2/2018 28/2/2018 1/3/2018 2/3/2018 5/3/2018 6/3/2018 7/3/2018 8/3/2018"
    # dates = "25/1/2018 26/1/2018"
    dates = dates.split(" ")

    print(dates)

    _docs = []
    _labels = []
    _ids = []
    _dates = []
    r_docs = []

    keys_fq = {}
    minhashes = {}
    keys_id = {}
    t = 0.55
    texts_ = []

    lsh = MinHashLSH(threshold=t, num_perm=256 )
    counter=0
    for d in dates:
        date= datetime.strptime(d, "%d/%m/%Y").strftime("%Y-%m-%d")
        print (date)
        data = label("GE", date)
        
        dataset = transformLabeledData(dataset=data)
        documents = dataset["documents"]
    
        ids = dataset["id"]
        texts = dataset["texts"]

        for i in range(len(documents)):
            
            doc = documents[i]
            if len(doc) <3:
                continue

            n_grams = ngrams(doc,2)
            ndoc = [ '_'.join(grams) for grams in n_grams]
            if len(ndoc)>0:
            
                doc_set = set(ndoc)
            
                lsh, minhashes, keys_fq, keys_id = lsh_main(lsh, doc_set, ids[i], minhashes, keys_fq, keys_id, counter= counter,t=t)
                for ii in keys_id.keys():
                    if ids[i] in keys_id[ii]:
                        
                        if keys_fq[ii] <=1:
                            # take keys fq outside so that it can be used on other part, duplicate tweet check and time gap
                            texts_.append(texts[i])
                            _docs.append(ndoc)
                            _ids.append(ids[i])
                        
                            _dates.append(date)
                            r_docs.append(doc)
            counter+=1

    # print (keys_fq) 
    # print (keys_id)

    tfidf = joblib.load("./pickles/tfidf_GE_v0_08_04_2018.pkl")
    vectors = tfidf.transform(_docs)

    iso_forest = joblib.load("./pickles/iso_GE_v0_08_04_2018.pkl")
    pred = iso_forest.predict(vectors)

    v2 = vectors
    vectors = vectors.toarray()

    now = None
    count = 0
    count_norm = 0
    n_labels = [] # all labels

    _vectors = []
    _r_docs = [] # general parsed documents
    # g_docs = []
    g_vec = [] # good document
    g_ids = [] # general case
    r_text = [] # general text

    for i in range(len(pred)):

        spam = spams.find({"id":_ids[i], "spam":True})
        neutral = sentiment.find({"id":_ids[i], "sentiment":"neutral"})
        if now!= _dates[i]:
            now = _dates[i]
            print (now)
       
        _r_docs.append(r_docs[i])
        # _r_docs.append(outlier_tokenizer(texts_[i]))
        g_ids.append(_ids[i])
        r_text.append(texts_[i])

        if pred[i]<0:
            # auto spam case
            _vectors.append(vectors[i])
            n_labels.append(-1)
            count+=1
        
        elif pred[i]>0 and spam.count()>0:
            # manual spam case
            # _r_docs.append(r_docs[i])
            _vectors.append(vectors[i])
            n_labels.append(-1)
            count+=1

        elif pred[i]>0 and spam.count()==0:

            if neutral.count() >0:
                # neutral case
                # _r_docs.append(r_docs[i])
                
                _vectors.append(vectors[i])
                n_labels.append(-1)
                count+=1
            else:
                count_norm+=1
                # g_docs.append(r_docs[i])
                g_vec.append(vectors[i])
                n_labels.append(1)
            
    print (count)
    print (count_norm)

    _vectors = np.array(_vectors)
    n_labels = np.array(n_labels)

    g_vec = np.array(g_vec)

    g_ids = np.array(g_ids)

    return _r_docs, g_ids, n_labels, r_text


def getTwitterAttibutes(idx):

    tweets = db2.djias
    t_col = tweets.find_one({"id":idx})
    
    truncated = t_col["status"]["truncated"]
    verified = t_col["status"]["user"]["verified"]
    friends = t_col["status"]["user"]["friends_count"]
    followers = int(t_col["status"]["user"]["followers_count"] )+1
    friends_followers_ratio = friends / followers 
    digits = 0
       
    if not truncated:
        text =  [ t.lower().strip() for t in t_col["status"]["text"].split(" ")]
            
        for t in text:
                
            for c in t:
                if c.isdigit():
                    digits+=1
                    break
            
        cashtags = [ ii["text"] for ii in t_col["status"]["entities"]["symbols"]]
        hashtag = [ ii["text"] for ii in t_col["status"]["entities"]["hashtags"]]
        url = [ ii["url"] for ii in  t_col["status"]["entities"]["urls"]]
        mentions = [ ii["screen_name"] for ii in t_col["status"]["entities"]["user_mentions"]]
            
    else:
        text =  [ t.strip() for t in t_col["status"]["extended_tweet"]["full_text"].split(" ")]
        
        for t in text:
            
            for c in t:
                if c.isdigit():
                    digits+=1
                    break
        
        cashtags = [ ii["text"] for ii in t_col["status"]["extended_tweet"]["entities"]["symbols"]]
        hashtag = [ ii["text"] for ii in t_col["status"]["extended_tweet"]["entities"]["hashtags"]]
        url = [ ii["url"] for ii in t_col["status"]["extended_tweet"]["entities"]["urls"]]
        mentions = [ ii["screen_name"] for ii in t_col["status"]["extended_tweet"]["entities"]["user_mentions"]]
        
    number_of_cashtag = len(cashtags)
    number_of_url = len(url)
    number_of_hashtag = len(hashtag)
    number_of_mention = len(mentions)

    return np.array([int(truncated), int( verified), number_of_cashtag, number_of_url, number_of_hashtag, number_of_mention, digits, friends_followers_ratio])

def generate_frame ():

    _r_docs, g_ids, n_labels, r_text = data()
    
    col2 = []
    for i in range(len(g_ids)):

        
        attributes = getTwitterAttibutes(g_ids[i])
        col2.append(attributes)
    
    col2 = np.array(col2)
    # dataframe = pd.DataFrame(np.column_stack([g_ids[:], col2[:,0], col2[:,1], col2[:,2], col2[:,3], col2[:,4], col2[:,5], col2[:,6] ,col2[:,7], n_labels[:]]), columns=["id","truncated", "verified", "number_of_cashtag", "number_of_url","number_of_hashtag", "number_of_mention", "digits", "followers_ratio", "label"])
    # dataframe.to_csv("./data/more_outlier3.csv",sep=",",encoding='utf-8',index=False)
    data_filtering(col2, n_labels, texts=r_text, g_ids = g_ids)

def data_filtering(vectors, n_labels, texts=None, g_ids=None):


    iso = IsolationForest(n_estimators=100, bootstrap=True, contamination=0.37, max_samples="auto", random_state=1199, n_jobs=2, verbose=1)
    iso.fit(vectors, n_labels)
    joblib.dump(iso,"./pickles/OutlierDataset/iso_text_feature_7_5_2018.pkl") #0.37
    pred = iso.predict(vectors)
    print(accuracy_score(n_labels, pred))
    print(precision_score(n_labels, pred))
    print(recall_score(n_labels, pred))
    print(f1_score(n_labels, pred))
    conf = confusion_matrix(n_labels, pred)
    print (conf)

    tn, fp, fn, tp = conf.ravel()
    print ((tn, fp, fn, tp))
    r_texts = []
    r_labels = []
    r_vec = []
    r_id = []
    for i in range(len(pred)):
        if pred[i] <0:
            r_texts.append(texts[i])
            r_labels.append(n_labels[i])
            r_vec.append(vectors[i])
            r_id.append(g_ids[i])
    
    r_vec = np.array(r_vec)
    r_id = np.array(r_id)
    r_labels = np.array(r_labels)
    r_texts = np.array(r_texts)
    dataframe = pd.DataFrame(np.column_stack([r_id[:], r_vec[:,0], r_vec[:,1], r_vec[:,2], r_vec[:,3], r_vec[:,4], r_vec[:,5], r_vec[:,6] ,r_vec[:,7], r_labels[:], r_texts[:]]), columns=["id","truncated", "verified", "number_of_cashtag", "number_of_url","number_of_hashtag", "number_of_mention", "digits", "followers_ratio", "label", "texts"])
    dataframe.to_csv("./data/more_iso_037_outliers.csv",sep=",",encoding='utf-8',index=False)

    


def text_transform(_r_docs, n_labels):
    vectorizer = TfidfVectorizer(max_df=1.0, min_df=10, analyzer=vector_analyser ,ngram_range=(1, 2))
    
    vectors = vectorizer.fit_transform(_r_docs)
    # joblib.dump(vectorizer,"./pickles/OutlierDataset/tfidf_4_5_2018.pkl")
    tfidf_feature_names = vectorizer.get_feature_names()
    # print (tfidf_feature_names)

    tfidf = dict(zip(tfidf_feature_names, vectorizer.idf_))
    # print (tfidf)
    # print(vectors.shape)

    """
        mutual information feature selection
    """
    # ch2 = SelectKBest(mutual_info_classif, k=600)
    # vectors = ch2.fit_transform(vectors, n_labels)
    # joblib.dump(ch2,"./pickles/OutlierDataset/selector600_5_5_2018.pkl")

    # 200
    lsa = TruncatedSVD(n_components=200, algorithm="randomized", random_state=1001)
    vectors = lsa.fit_transform(vectors, n_labels)
    joblib.dump(lsa,"./pickles/OutlierDataset/lsa_6_5_2018.pkl")
    return vectors, n_labels

def main():
    
    _r_docs, g_ids, n_labels, r_text = data()

    # n_topics = 20
    # n_top_words = 10
    # nmf  = NMF(n_components=n_topics, random_state=1,alpha=.1, l1_ratio=.5).fit(_vectors)

    # print_top_words(nmf, tfidf_feature_names, n_top_words)
    # print (nmf.components_)


    # dbs = DBSCAN(eps=0.8, min_samples=10, metric="cosine", algorithm="auto", leaf_size=30, p=None, n_jobs=2)
    # dbs.fit(v2)

    # one = OneClassSVM(kernel="rbf", gamma=0.1, nu=0.5, verbose=1, random_state=1123 )
    # one.fit(vectors, n_labels)
    # pred = one.predict(vectors)

    # lof = LocalOutlierFactor(n_neighbors=20, algorithm="auto",  metric="cosine", contamination=0.1, n_jobs=2)
    # pred = lof.fit_predict(vectors, n_labels)

    vectors, n_labels = text_transform(_r_docs, n_labels)

    # X_train, X_test, y_train, y_test = train_test_split( vectors, n_labels, test_size=0.3, random_state=42)
    iso = IsolationForest(n_estimators=200, bootstrap=True, contamination=0.3, max_samples="auto", random_state=1199, n_jobs=2, verbose=1)
    iso.fit(vectors, n_labels)
    joblib.dump(iso,"./pickles/OutlierDataset/iso_5_5_2018.pkl")
    pred = iso.predict(vectors)
    print(accuracy_score(n_labels, pred))
    print(precision_score(n_labels, pred))
    print(recall_score(n_labels, pred))
    print(f1_score(n_labels, pred))
    conf = confusion_matrix(n_labels, pred)
    print (conf)

    tn, fp, fn, tp = conf.ravel()
    print ((tn, fp, fn, tp))

    res = []
    counter = 0
    for i in range(len(pred)):
        if (pred[i]<0):
            print(r_text[i], pred[i])
            print() 
            res.append({"sentiment": "outlier", "tokens":_r_docs[i], "id": g_ids[i]})
            counter+=1

    print("novelties ",counter)


def print_result(res):
    with open('/Users/williamhenry/Desktop/data.json', 'w') as outfile:
        print("printing result")
        json.dump(res, outfile)


def unused_inspectors():

    """
        Twitter inspection part
    """

    """
    X_neg = []
    X_pos = []

    y_neg = []
    y_pos = []

    vectors = vectors.toarray()

  
    for i in range(len(n_labels)):
        # positive has "0" label
        if n_labels[i] == 0:
        
            X_pos.append(vectors[i])
            y_pos.append(n_labels[i])
        else:
        
            X_neg.append(vectors[i])
            y_neg.append(n_labels[i])

    X1 , y1 = resample(X_pos, y_pos, replace=False, n_samples = 1500, random_state=1000)

    X = np.vstack((X1, X_neg))
    y = np.concatenate((y1, y_neg))
    """



    """
    X_train, X_test, y_train, y_test = train_test_split( X, y, test_size=0.3, random_state=42)
    X1_train, X1_test, y1_train, y1_test = train_test_split( vectors, n_labels, test_size=0.3, random_state=42)
    X_resampled, y_resampled = SMOTE(ratio='minority', k_neighbors=3, kind='svm', random_state=1011).fit_sample(X1_train, y1_train)
    

    lda = LinearDiscriminantAnalysis(solver="svd")
    lda.fit(X_train, y_train)

    pred  = lda.predict(X_test)


    # print (pred.tolist())
    # print (y_test.tolist())
    print(accuracy_score(y_test, pred))
    print(precision_score(y_test, pred))
    print(recall_score(y_test, pred))
    print(f1_score(y_test, pred))
    conf = confusion_matrix(y_test, pred)
    print (conf)

    tn, fp, fn, tp = conf.ravel()
    print ((tn, fp, fn, tp))

    print (np.mean(cross_val_score(lda, X_train, y_train, cv=10)))
    """
    """
        Random forest magic trick:
        min_samples_leaf=3 ==> cv: 0.7056103273046076
        min_samples_leaf=5 ==> cv: ~0.7

    """
    """
    rf = RandomForestClassifier(max_features = "auto",  min_samples_leaf=60, n_estimators=100, criterion="entropy",
            oob_score=True, bootstrap=True, random_state=1190, verbose=1)

    rf.fit(X_resampled, y_resampled)

    pred  = rf.predict(X1_test)


    # print (pred.tolist())
    # print (y_test.tolist())
    print(accuracy_score(y1_test, pred))
    print(precision_score(y1_test, pred))
    print(recall_score(y1_test, pred))
    print(f1_score(y1_test, pred))
    conf = confusion_matrix(y1_test, pred)
    print (conf)

    tn, fp, fn, tp = conf.ravel()
    print ((tn, fp, fn, tp))

    print (np.mean(cross_val_score(rf, X_train, y_train, cv=10)))
    joblib.dump(rf,"./pickles/OutlierDataset/rf_4_5_2018.pkl")
    # print ()
    # print("ada boost")
    # # significantly unbalanced
    # ada = AdaBoostClassifier(base_estimator=LinearDiscriminantAnalysis, n_estimators=1000, random_state=1027)
    # ada.fit(X1_train, y1_train)
    # pred  = ada.predict(X1_test)
    # print(accuracy_score(y1_test, pred))
    # print(precision_score(y1_test, pred))
    # print(recall_score(y1_test, pred))
    # print(f1_score(y1_test, pred))
    # conf = confusion_matrix(y1_test, pred)
    # print (conf)

    # tn, fp, fn, tp = conf.ravel()
    # print ((tn, fp, fn, tp))

    # print (np.mean(cross_val_score(ada, X1_train, y1_train, cv=10)))

    """
    pass

# main()

# generate_frame()
