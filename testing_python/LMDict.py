import pandas as pd

def getPositives():

    dataframe = pd.read_csv("./WSD/LoughranMcDonald_Positive.csv",usecols=[0])
    arr = dataframe.as_matrix().tolist()
    return arr

def getNegatives():
    
    dataframe = pd.read_csv("./WSD/LoughranMcDonald_Negative.csv",usecols=[0])
    arr = dataframe.as_matrix().tolist()
    return arr

def getModalStrong():
    
    dataframe = pd.read_csv("./WSD/LoughranMcDonald_ModalStrong.csv",usecols=[0])
    arr = dataframe.as_matrix().tolist()
    return arr

def getModalWeak():
    
    dataframe = pd.read_csv("./WSD/LoughranMcDonald_ModalWeak.csv",usecols=[0])
    arr = dataframe.as_matrix().tolist()
    return arr

def getUncertainty():
    
    dataframe = pd.read_csv("./WSD/LoughranMcDonald_Uncertainty.csv",usecols=[0])
    arr = dataframe.as_matrix().tolist()
    return arr

def getSentimentScore(corpus):
    print (corpus)
    pos = getPositives()
    print(pos)
    neg = getNegatives()
    strong = getModalStrong()
    weak = getModalWeak()

    uncertainty = getUncertainty()

    cPos = []
    cNeg = []
    cUncertainty = []
    cStrong = []
    cWeak = []
    for i in corpus:
        print (i)
        if [i.upper()] in pos:
            cPos.append(i.upper())
        if [i.upper()] in neg:
            cNeg.append(i.upper())
        if [i.upper()] in uncertainty:
            cUncertainty.append(i.upper())
        if [i.upper()] in strong:
            cStrong.append(i.upper)
        if [i.upper()] in weak:
            cWeak.append(i.upper)

    return (2*len(cPos)) + len(cStrong) - (2*len(cNeg)) - len(cWeak) - len(cUncertainty)

def checkWords(words):

    pos = getPositives()
   
    neg = getNegatives()
    strong = getModalStrong()
    weak = getModalWeak()
    uncertain = getUncertainty()

    for i in words:

        word = [i.upper()]
        if word in pos:
            return True
        elif word in neg:
            return True
        elif word in weak:
            return True
        elif word in strong:
            return True
        elif word in uncertain:
            return True

    return False



# print(getSentimentScore("tremendous".split()))
