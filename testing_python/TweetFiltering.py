from nltk.util import ngrams
from hdbscan import HDBSCAN

from sklearn.cluster import DBSCAN
from sklearn.decomposition import TruncatedSVD, PCA
from sklearn.preprocessing import Normalizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import IsolationForest
from sklearn import metrics
from sklearn.model_selection import train_test_split

import logging
import numpy as np

# import matplotlib.pyplot as plt
import DatasetUtils
from TwitterData import label
from sklearn.externals import joblib

def n_grams_analyser(x):
    return x


def filter():
      l = label("GE", date=None)
      dataset = DatasetUtils.transformLabeledData(dataset=l, symbol=None)

      # print (dataset)
      documents = dataset["documents"]
      labels = dataset["labels"]
      ids = dataset["id"]


      docs=[]
      for doc in documents:
            n_grams = ngrams(doc,2)
            ndoc = [ '_'.join(grams) for grams in n_grams]
            if len(ndoc)>0:
                  docs.append(ndoc)

      documents = docs

      # print (documents)

      tfidf_vectorizer = TfidfVectorizer(max_df=1.0, min_df=5, max_features=200, analyzer=n_grams_analyser,ngram_range=(1, 3))

      X = tfidf_vectorizer.fit_transform(documents)
      # joblib.dump(tfidf_vectorizer,"./pickles/tfidf_GE_v0_08_04_2018.pkl")

      print("n_samples: %d, n_features: %d" % X.shape)

      pca = PCA(n_components=4)

      lbl = []
      for i in documents:
            lbl.append(0)

      indices = np.arange(len(documents))

      # the outlier detection
      trainX, testX, trainY, testY, idx1, idx2 = train_test_split( X, lbl , indices,test_size=0.3, random_state=1057) #42
      iso_forest = IsolationForest(verbose=1, max_samples="auto", n_estimators=100, bootstrap=True, n_jobs=2, max_features=1.0, random_state=1001, contamination=0.25)
      iso_forest.fit(trainX)
      # joblib.dump(iso_forest,"./pickles/iso_GE_v0_08_04_2018.pkl")
      # print(idx1.shape)
      # print(idx2.shape)
      pred = iso_forest.predict(testX)
      counter = 0
      for i in range(len(pred)):
            if pred[i] == -1:
                  idx = idx2[i]
                  print(documents[idx])
                  counter+=1


      print()
      print (counter)


# filter()
