import json
from pymongo import MongoClient
from pprint import pprint

with open('/Users/williamhenry/Desktop/mongo_dump/mongo_old_dump_08_03_2018.json') as f:
    data = json.load(f)

print (len(data))

client = MongoClient('localhost',27017)

db = client.tweets
collection = db.stream_gcp_2

# a = collection.find({})

# a = list(a)

# a = sorted(a, key = lambda x: x["status"]["timestamp_ms"], reverse=True)
# max_t = a[0]["status"]["timestamp_ms"]

for i in data:
    obj = {}
    obj['id']= i['id']
    obj['status']= i['status']
    obj['tickers']=i['tickers']
    # print(i["status"]["timestamp_ms"])
    # if int(i["status"]["timestamp_ms"]) > int(max_t):
    #     print(i["id"])
    collection.insert(obj)
